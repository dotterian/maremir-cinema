import { ConnectionOptions, createConnection } from 'typeorm';
import Achievement from './models/Achievement';
import User from './models/User';
import Film from './models/Film';
import Voting from './models/Voting';


const options: ConnectionOptions = {
  type: 'sqlite',
  database: './common/db',
  entities: [
    Achievement,
    Film,
    User,
    Voting
  ],
  logging: ['error']
}

export default async () => {
  const connection = await createConnection(options)
  return connection
}
