import Voting, { FilmInfo } from '../common/models/Voting'
import Film from '../common/models/Film'
import { getManager, Not } from 'typeorm';
import { Answer } from './types';

export default async (date: string) => {
  const manager = getManager()
  let poll = await manager.findOne(Voting, { date });
  let answers: Answer[] = [];
  let votedIds: string[] = [];

  if (!poll) {
    poll = await createNewPoll(date)
  }

  votedIds = poll.voted

  for (let i = 0; i < poll.films.length; i += 1) {
    let item = poll.films[i];
    let film = await manager.findOne(Film, item.film_id)

    answers[i] = {
      ...film,
      votes: item.votes,
    }
  }

  return {
    answers,
    votedIds
  }
}

const createNewPoll = async (date: string) => {
  const answers: FilmInfo[] = []
  const manager = getManager()
  let films = await manager.find(Film, {
    where: {
      watched: Not(true)
    }
  })
  let filmsByUser: { [key: string]: { id: number }[] } = {}
  let userIds: string[] = []

  films.map((film) => {
    if (!filmsByUser.hasOwnProperty(film.user_uuid)) {
      filmsByUser[film.user_uuid] = [];
      userIds.push(film.user_uuid)
    }

    filmsByUser[film.user_uuid].push({
      id: film.id
    })
  })

  for (let i = 0; i < 10; i += 1) {
    if (userIds.length === 0) {
      break;
    }

    let userId = userIds[Math.floor(Math.random() * userIds.length)];
    let film = filmsByUser[userId][Math.floor(Math.random() * filmsByUser[userId].length)]

    answers.push({
      film_id: film.id,
      user_id: userId,
      votes: 0
    })

    userIds.splice(userIds.indexOf(userId), 1)
  }

  const poll = manager.create(Voting, {
    films: answers,
    date: date,
    voted: []
  })
  await manager.save(poll)

  return poll;
}
