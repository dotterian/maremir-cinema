const formatDate = (date: Date) => {
  const d = date.getDate()
  let mm = date.getMonth() + 1
  let mmStr = `${mm}`
  const yyyy = date.getFullYear()

  if (mm < 10) {
    mmStr = `0${mm}`
  }
  return `${d}.${mmStr}.${yyyy}`
}

export const formatDateSql = (date: Date) => {
  let mm = `${date.getMonth() + 1}`
  let dd = `${date.getDate()}`
  if (mm.length < 2) {
    mm = `0${mm}`
  }
  if (dd.length < 2) {
    dd = `0${dd}`
  }
  return `${date.getFullYear()}-${mm}-${dd}`
}

export const getNextWednesday = ():[string, number] => {
  const today = new Date()
  const nextDate = new Date()
  const deltaDays = (10 - today.getDay()) % 7
  nextDate.setDate(today.getDate() + deltaDays)
  return [formatDate(nextDate), deltaDays]
}

export const getToday = ():[string, number] => {
  return [formatDate(new Date()), 0]
}
