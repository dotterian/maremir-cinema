export function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

export function randomArrayElement<T>(array: Array<T>): T {
  shuffle(array)
  return array[Math.floor(Math.random() * array.length)]
}

const cases = [2, 0, 1, 1, 1, 2];
export type declofNumSubFunction = (number: number) => string
const declofNumSubFunction = (titles: string[], number: number) => {
  number = Math.abs(number)
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]]
}
export function declOfNum(titles: string[]): declofNumSubFunction
export function declOfNum(titles: string[], number: number): string
export function declOfNum(titles: string[]) {
    if (arguments.length === 1) {
      return function (number: number) {
        return declofNumSubFunction(titles, number)
      }
    } else {
      return declofNumSubFunction.apply(null, arguments)
    }
}
