import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

export type FilmInfo = {
  film_id: number,
  user_id: string,
  votes: number
}

@Entity('voting')
export default class Voting {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  date?: string

  @Column({
    type: 'simple-json'
  })
  voted?: string[]

  @Column({
    type: 'simple-json'
  })
  films: FilmInfo[]
}
