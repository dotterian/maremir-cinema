import User from './User'
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, JoinColumn } from 'typeorm';

@Entity('achievement')
export default class Achievement {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  name?: string

  @Column()
  text?: string

  @Column()
  given_at?: string

  @Column()
  user?: string

  @Column()
  given_by?: string

  @ManyToOne(type => User)
  @JoinColumn({ name: 'user' })
  User?: User

  @ManyToOne(type => User)
  @JoinColumn({ name: 'given_by' })
  givenBy?: User
}
