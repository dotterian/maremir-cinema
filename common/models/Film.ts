import User from './User'
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';

@Entity('film')
export default class Film {

  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  title?: string

  @Column()
  kp_link?: string

  @Column()
  watched?: boolean

  @Column()
  date_watched?: string

  @Column()
  poster?: string

  @Column()
  user_uuid?: string

  @ManyToOne(type => User)
  @JoinColumn({ name: 'user_uuid' })
  user?: User
}
