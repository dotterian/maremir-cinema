import Film from './Film'
import Achievement from './Achievement'
import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm';
import { UserType } from '../types';

@Entity('user')
export default class User {

  public static get nullUser(): UserType {
    return {
      id: null,
      name: null,
      token: null,
      isMember: false
    }
  }

  @PrimaryColumn()
  guid!: string

  @Column()
  name?: string

  @Column()
  words_today?: number

  @Column()
  words_total?: number

  @Column()
  today?: string

  @Column()
  sudak_counter?: number

  @Column()
  gender?: boolean

  @OneToMany(type => Achievement, achievement => achievement.User)
  achievements?: Achievement[]

  @OneToMany(type => Achievement, achievement => achievement.givenBy)
  givenAchivements?: Achievement[]

  @OneToMany(type => Film, film => film.user)
  films?: Film[]
}
