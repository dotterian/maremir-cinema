import { User } from './User'

export type Achievement = {
  id: number,
  name?: string,
  text?: string,
  given_at?: string,
  user?: string,
  given_by?: string,
  User?: User,
  givenBy?: User
}
