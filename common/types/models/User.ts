import UserType from './UserType';
import { Achievement } from './Achievement';
import { Film } from './Film';

export type User = {
  nullUser?: UserType
  guid: string,
  name?: string,
  words_today?: number,
  words_total?: number,
  today?: string,
  sudak_counter?: number,
  gender?: boolean,
  achievements?: Achievement[],
  givenAchievements?: Achievement[],
  films?: Film[]
}
