export type FilmInfo = {
  film_id: number,
  user_id: string,
  votes: number
}

export type Voting = {
  id: number,
  date: string,
  voted: string[],
  films: FilmInfo[]
}
