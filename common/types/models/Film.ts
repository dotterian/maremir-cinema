import { User } from './User'

export type Film = {
  id: number,
  title?: string,
  kp_link?: string,
  watched?: boolean,
  date_watched?: string,
  poster?: string,
  user_uuid?: string,
  user?: User
}
