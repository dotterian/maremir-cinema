import { Film } from '../models/Film';

type Answer = Film & {
  votes: number
}

export default Answer
