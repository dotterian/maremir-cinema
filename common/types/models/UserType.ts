type UserType = {
  id: string,
  name: string,
  token: string,
  isMember: boolean
}

export default UserType
