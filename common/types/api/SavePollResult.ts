
type ApiSavePollResult = {
  saved: boolean
}

export default ApiSavePollResult
