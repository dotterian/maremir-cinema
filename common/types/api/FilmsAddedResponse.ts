import { Film } from '../models/Film';

type FilmsAddedResponse = Film[]

export default FilmsAddedResponse
