import { UserType, Answer } from '../'

type ApiFetchPollResult = {
  nextDate: string,
  pollActive: boolean,
  voted: boolean,
  loginUrl: string,
  user: UserType,
  pollResults: {
    today: boolean,
    kp_link?: string,
    title?: string,
    user?: {
      guid: string,
      name?: string
    }
  },
  answers: Answer[],
  votedIds: string[],
  voteCount: number
}

export default ApiFetchPollResult
