type UsersResponse = Array<{
  guid: string,
  name: string,
  today: string,
  words_today: number,
  words_total: number,
  sudak_counter: number,
  achievements: number
}>

export default UsersResponse
