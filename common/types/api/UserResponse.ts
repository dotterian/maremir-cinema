import { Achievement } from '../models/Achievement';

type UserResponse = {
  name: string,
  loggedIn: boolean,
  canDelete: boolean,
  loginUrl: string,
  achievements: Achievement[]
}

export default UserResponse
