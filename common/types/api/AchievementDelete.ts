import { Achievement } from '../models/Achievement';

type AchievementDelete = {
  achievements: Achievement[]
}

export default AchievementDelete
