import { Film } from '../models/Film';

type FilmsWatchedResponse = Film[]

export default FilmsWatchedResponse
