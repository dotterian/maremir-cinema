import { Achievement } from '../models/Achievement';

type Achievements = {
  last: Achievement[],
  full: Array<{
    title: string,
    length: number,
    items: Achievement[]
  }>
}

export default Achievements
