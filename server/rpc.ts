import * as rpc from 'node-json-rpc2'

export default class RPC {
  private static _rpc: rpc.Client = null

  public static getInstance() {
    if (!RPC._rpc) {
      const rpcOptions = {
        port: 5711,
        host: '127.0.0.1',
        path: '/',
        method: 'POST'
      }
      RPC._rpc = new rpc.Client(rpcOptions)
    }
    return RPC._rpc;
  }
}
