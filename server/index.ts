import * as Koa from 'koa';
import * as Static from 'koa-static'
import * as bodyParser from 'koa-bodyparser';
import * as fileSession from 'koa-file-session';
import * as session from 'koa-session';
import * as Parcel from 'parcel-bundler'
import * as Socket from 'socket.io'
import { resolve, basename, extname } from 'path';
import { parse } from 'url'
import 'reflect-metadata';
import router from './routers';
import createConnection from '../common/database'

const port = process.env.PORT || 8080

const parcel = new Parcel('./client/index.html')
parcel.bundle()
const serve = Static(parcel.options.outDir)
const parcelMiddleware: Koa.Middleware = async (ctx, next) =>
  new Promise((resolve, reject) => {
    if (parcel.pending) {
      parcel.once('bundled', respond)
    } else {
      respond()
    }

    function respond() {
      const { pathname } = parse(ctx.url)
      if (parcel.error || pathname === undefined) {
        ctx.throw(500, pathname === undefined ? 'pathname === undefined' : parcel.error)
      } else if (!pathname.startsWith(parcel.options.publicURL) || extname(pathname) === '') {
        ctx.url = `/${basename(parcel.mainBundle.name)}`
        return resolve(serve(ctx, next))
      } else {
        ctx.url = pathname.slice(parcel.options.publicURL.length)
        return resolve(serve(ctx, next))
      }

      reject(new Error('Unhandled case'))
    }
})
let userCounter = 0
let connectedUsers = []

createConnection()
  .then(() => {
    const app = new Koa()
    const io = Socket(1488, {
      serveClient: false
    })
    io.on('connect', (socket) => {
      const user = null
      if (!user || !connectedUsers.includes(user)) {
        userCounter += 1
        if (user) {
          connectedUsers.push(user)
        }
        io.sockets.emit('updateUserCounter', userCounter)
        socket.on('disconnect', () => {
          userCounter -= 1
          if (user) {
            connectedUsers.splice(connectedUsers.indexOf(user), 1)
          }
          io.sockets.emit('updateUserCounter', userCounter)
        })
      }
    })
    app.keys = ['maremir']
    app
      .use(session(app))
      .use(fileSession(app, {
        directory: resolve('./server/data/session'),
        key: 'SESSION_ID',
        maxAge: 0
      }))
      .use(bodyParser({
        enableTypes: ['json', 'urlencoded']
      }))
      .use(router.routes())
      .use(router.allowedMethods())
      .use(parcelMiddleware)

    app.listen(port)
    console.log('Server successfully started')
  })
  .catch(err => {
    console.error(err)
  })


