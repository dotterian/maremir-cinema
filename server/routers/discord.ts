import * as Router from 'koa-router'
import fetch from 'node-fetch'
import User from '../../common/models/User'
import { getManager } from 'typeorm';

const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const serverName = process.env.SERVER_NAME || 'localhost:8080'
const redirect = encodeURIComponent(`http://${serverName}/api/discord/callback`)
const guildId = '416670606356905984'

const router = new Router();

router
    .get('/login', (ctx) => {
        if (!CLIENT_ID) {
            ctx.body = 'Invalid configuration: CLIENT_ID is not defined'
            return
        }
        ctx.redirect(`https://discord.com/api/oauth2/authorize?client_id=${CLIENT_ID}&redirect_uri=${redirect}&response_type=code&scope=identify%20guilds`)
    })
    .get('/callback', async (ctx) => {
        if (!ctx.query.code) {
            throw new Error('NoCodeProvided')
        }
        if (!CLIENT_ID) {
            ctx.body = 'Invalid configuration: CLIENT_ID is not defined'
            return
        }
        if (!CLIENT_SECRET) {
            ctx.body = 'Invalid configuration: CLIENT_SECRET is not defined'
            return
        }
        const code = ctx.query.code
        const creds = Buffer.from(`${CLIENT_ID}:${CLIENT_SECRET}`, 'binary').toString('base64')
        const user = User.nullUser
        const accessTokenRequest = await fetch(`https://discord.com/api/oauth2/token`, {
            method: 'POST',
            headers: {
                Authorization: `Basic ${creds}`
            },
            body: JSON.stringify({
                grant_type: 'authorization_code',
                code,
                redirect_uri: redirect
            })
        })
            .then(response => response.json())
        user.token = accessTokenRequest.access_token

        console.log(accessTokenRequest)

        const guilds = await fetch(`https://discord.com/api/v6/users/@me/guilds`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(response => response.json())

        for (let guild of guilds) {
            if (guild.id === guildId) {
                user.isMember = true
                break
            }
        }
        if (user.isMember) {
            const userInfo = await fetch(`https://discord.com/api/v6/users/@me`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${user.token}`
                }
            })
                .then(response => response.json())
            user.id = userInfo.id
            user.name = userInfo.username + '#' + userInfo.discriminator

            const manager = getManager();
            let dbUser = await manager.findOne(User, userInfo.id)
            if (dbUser) {
                user.name = dbUser.name
            }
        }
        if (user.name && user.isMember) {
            console.log(`Discord authorization:\nUser: ${user.name}\nGuildMember: ${user.isMember}\nGuilds:`)
        }
        console.log(guilds)
        ctx.fileSession.set('user', user)
        ctx.redirect(`/`)
    })

export default router;
