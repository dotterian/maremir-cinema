import * as Router from 'koa-router'
import { getNextWednesday } from '../../common/dateTools'
import Film from '../../common/models/Film'
import User from '../../common/models/User'
import getPoll from '../../common/getPoll'
import Voting from '../../common/models/Voting'
import RPC from '../rpc'
import { getManager } from 'typeorm';
import { ApiFetchPollResult, UserType, ApiSavePollResult } from '../../common/types';

const serverName = process.env.SERVER_NAME || 'localhost:8080'
const RpcClient = RPC.getInstance()

const router = new Router()

router
  .get('/', async (ctx) => {
    const loginUrl = `http://${serverName}/api/discord/login`
    const today = new Date()
    const [date, deltaDays] = getNextWednesday()
    let voted = true

    // Get last watched film
    const manager = getManager()
    let film = await manager.findOne(Film, {
      order: {
        date_watched: 'DESC'
      },
      relations: [
        'user'
      ]
    })

    // Populate response
    let poll: ApiFetchPollResult = {
      nextDate: date,
      pollActive: false,
      voted: false,
      loginUrl,
      user: User.nullUser,
      pollResults: {
        today: deltaDays === 0 && today.getHours() >= 20,
        ...film
      },
      answers: [],
      votedIds: [],
      voteCount: 0
    }

    // Get user from session
    const user: UserType = ctx.fileSession.get('user') || User.nullUser

    if (today.getDay() <= 3 || today.getDay() >= 6) {
      if (today.getDay() !== 3 || today.getHours() < 20) {
        const { answers, votedIds } = await getPoll(date)
        poll = {
          ...poll,
          pollActive: true,
          answers,
          votedIds
        }
      }
    }

    if (user.isMember) {
      if (!poll.votedIds.includes(user.id)) {
        voted = false
      }
    }

    const countVoted = poll.votedIds.length || 0

    // for (let answer of poll.answers) {
    //   answer.percentage = Math.round(answer.votes * 100 / countVoted) || 0
    // }

    poll = {
      ...poll,
      voted,
      user,
      voteCount: countVoted
    }
    ctx.body = poll
  })
  .post('/', async (ctx) => {
    const [date] = getNextWednesday()
    const manager = getManager()
    const poll = await manager.findOne(Voting, {
      date: date
    })
    const votedIds = poll.voted
    const films = poll.films
    const answers = ctx.request.body.answers

    const user: UserType = ctx.fileSession.get('user') || User.nullUser
    const filmNames = []

    if (user.id !== null && votedIds.indexOf(user.id) === -1) {
      votedIds.push(user.id)
      for (let i in films) {
        if (films.hasOwnProperty(i)) {
          if (answers.indexOf(`${films[i].film_id}`) !== -1) {
            films[i].votes += 1
            let film = await manager.findOne(Film, films[i].film_id)
            filmNames.push(film.title)
          }
        }
      }
      await manager.update(Voting, {
        date
      }, {
        films,
        voted: votedIds
      })

      RpcClient.call({
        method: 'vote',
        params: [
          user.name,
          filmNames
        ]
      }, (err: Error) => {
        if (err) {
          console.error(err.message)
        }
      })

      ctx.body = <ApiSavePollResult>{ saved: true }
    } else {
      ctx.body = <ApiSavePollResult>{ saved: false }
    }
  })

export default router
