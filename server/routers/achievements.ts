import * as Router from 'koa-router'
import Achievement from '../../common/models/Achievement';
import User from '../../common/models/User';
import { getManager } from 'typeorm';
import { ApiAchievements } from '../../common/types';

const router = new Router()

router.delete('/:id', async (ctx) => {
    const id = ctx.params.id
    const user = ctx.fileSession.get('user') || User.nullUser

    const manager = getManager()
    const achievement = await manager.findOne(Achievement, id, {
      relations: [
        'User'
      ]
    })

    if (!achievement) {
      ctx.throw(404)
      return
    }

    if (user.id != achievement.User.guid && user.id != '284746199918051330') {
      ctx.throw(401)
      return
    }

    const guid = achievement.User.guid
    manager.delete(Achievement, id)
    const achievements = await manager.find(Achievement, {
      where: {
        user: guid
      },
      order: {
        id: 'DESC'
      },
      relations: [
        'givenBy'
      ]
    })

    ctx.body = { achievements }
  })
  .get('/', async (ctx) => {
    const manager = getManager()
    const last: ApiAchievements['last'] = await manager.find(Achievement, {
      order: {
        id: 'DESC'
      },
      take: 10,
      relations: [
        'User',
        'givenBy'
      ]
    })

    const achievements = await manager.find(Achievement, {
      relations: [
        'User'
      ]
    })
      .then(async r => {
        const newArray = {}

        for (let item of r) {
          let key = item.name.toLowerCase()
          if (!newArray.hasOwnProperty(key)) {
            newArray[key] = []
          }
          newArray[key].push(item)
        }

        return newArray
      })

    const full: ApiAchievements['full'] = []

    for (let item in achievements) {
      full.push({
        title: item,
        length: achievements[item].length,
        items: achievements[item]
      })
    }

    ctx.body = <ApiAchievements>{ last, full }
  })

  export default router
