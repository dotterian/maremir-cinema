import * as Router from 'koa-router'

import MainRouter from './main'
import PollRouter from './poll'
import FilmsRouter from './films'
import DiscordRouter from './discord'
import NewPageRouter from './new-page'
import UsersRouter from './user'
import AchievementsRouter from './achievements'

const router = new Router();
const apiRouter = new Router();

apiRouter
  .use('/poll', PollRouter.routes(), PollRouter.allowedMethods())
  .use('/films', FilmsRouter.routes(), FilmsRouter.allowedMethods())
  .use('/discord', DiscordRouter.routes(), DiscordRouter.allowedMethods())
  .use('/users', UsersRouter.routes(), UsersRouter.allowedMethods())
  .use('/achievements', AchievementsRouter.routes(), AchievementsRouter.allowedMethods())
  .use('/new-page', NewPageRouter.routes(), NewPageRouter.allowedMethods())
  .use('/', MainRouter.routes(), MainRouter.allowedMethods())

router
  .use('/api', apiRouter.routes(), apiRouter.allowedMethods())

export default router;
