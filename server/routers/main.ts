import * as Router from 'koa-router'
import { exists, readFile } from 'async-file'
import { Converter } from 'showdown'
import { ApiPageResponse } from '../../common/types';

const router = new Router();

router
  .get('page/:page', async (ctx) => {
    const fileName = `./server/data/${ctx.params.page}.md`
    const exist = await exists(fileName)

    if (!exist) {
      ctx.throw(404)
      return
    }

    const page = await readFile(fileName)
    const converter = new Converter()
    const processedPage = converter.makeHtml(page.toString())

    ctx.body = <ApiPageResponse>{ text: processedPage }
  })
  .get('logout', (ctx) => {
    ctx.fileSession.clear()
    ctx.redirect('/')
  })
  .get('*', (ctx) => {
    ctx.throw(404, {
      data: {
        error: "NOT_FOUND",
        message: "Endpoint doesn't exist"
      }
    })
  })

export default router
