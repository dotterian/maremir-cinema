import * as Router from 'koa-router'
import User from '../../common/models/User'
import { getManager } from 'typeorm';
import { formatDateSql } from '../../common/dateTools';
import { ApiUsersResponse } from '../../common/types';
const serverName = process.env.SERVER_NAME || 'localhost:8080'

const router = new Router()

router
  .get('/:uuid', async (ctx) => {
    const uuid = ctx.params.uuid
    const loginUrl = `http://${serverName}/api/discord/login`
    let canDelete = false
    let loggedIn = false
    const sessionUser = ctx.fileSession.get('user') || User.nullUser

    if (sessionUser.id) {
      loggedIn = true
    }

    const manager = getManager()
    const user = await manager.findOne(User, uuid, {
      relations: [
        'achievements',
        'achievements.givenBy'
      ],
    })
    if (!user) {
      ctx.throw(404)
      return
    }

    if (sessionUser.id == user.guid || sessionUser.id == '284746199918051330') {
      canDelete = true
    }

    ctx.body = {
      name: user.name,
      loggedIn,
      canDelete,
      loginUrl,
      achievements: user.achievements,
    }
  })
  .get('/', async (ctx) => {
    const manager = getManager()
    const users = await manager.find(User, {
      relations: [
        'achievements'
      ]
    })

    const newUsers = JSON.parse(JSON.stringify(users))

    for (let user of newUsers) {
      if (user.today !== formatDateSql(new Date())) {
        user.words_today = 0
      }
      if (!user.words_total) {
        user.words_total = 0
      }
      user.achievements = user.achievements.length
    }
    ctx.body = <ApiUsersResponse>newUsers
  })

export default router;
