import * as Router from 'koa-router'
import fetch from 'node-fetch'
import Film from '../../common/models/Film'
import User from '../../common/models/User'
import { getManager, Not } from 'typeorm';
import { ApiFilmsWatchedResponse, ApiFilmsAddedResponse } from '../../common/types';

const router = new Router();

router
  .get('/watched', async (ctx) => {
    const manager = getManager()
    const watched = await manager.find(Film, {
      where: {
        watched: true
      },
      order: {
        date_watched: 'DESC'
      },
      relations: [
        'user'
      ]
    })

    ctx.body = <ApiFilmsWatchedResponse>watched
  })
  .get('/added', async (ctx) => {
    const manager = getManager()
    const added = await manager.find(Film, {
      where: {
        watched: Not(true)
      },
      order: {
        title: 'ASC'
      },
      relations: [
        'user'
      ]
    })

    ctx.body = <ApiFilmsAddedResponse>added
  })
  .get('/watching', async (ctx) => {
    try {
      const result = await fetch('https://cinema.dotterian.ru/live/cinema.m3u8')
      if (result.ok) {
        ctx.body = { watching: true }
      } else {
        ctx.body = { watching: false }
      }
    } catch (err) {
      ctx.body = { watching: false }
    }
  })

export default router
