import * as Router from 'koa-router'
import RPC from '../rpc'

const router = new Router()
const RpcClient = RPC.getInstance()

router
  .post('/', async (ctx) => {
    try {
      await new Promise((resolve, reject) => {
        RpcClient.call({
          method: 'newPage',
          params: [
            ctx.request.body.link,
            ctx.request.body.excerpt,
            ctx.request.body.image
          ]
        }, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        })
      })
      ctx.body = { result: 'ok' }
      return
    } catch (err) {
      ctx.body = { result: 'error' }
      console.error(err)
    }
  })

export default router
