import * as React from 'react'
import { store } from 'react-easy-state'

type Storage = {
  lightbox: boolean,
  lightboxImage: string,
  popup: boolean,
  popupChildren: React.ReactNode
}

const storage: Storage = store({
  lightbox: false,
  lightboxImage: '',
  popup: false,
  popupChildren: ''
})

export default storage
