import * as React from 'react'
import { Route, RouteComponentProps, RouteProps } from 'react-router-dom'
import { Menu } from '../components'

type PropTypes = {
  component: React.ComponentType<RouteComponentProps>
} & RouteProps

export default class DefaultLayout extends React.Component<PropTypes> {
  public render() {
    const { component: Component, ...rest } = this.props
    return <Route {...rest} render={
      (matchProps) => <div className='App'>
        <header className='App-header' />
        <Menu {...matchProps} />
        <Component {...matchProps} />
      </div>
    } />
  }
}
