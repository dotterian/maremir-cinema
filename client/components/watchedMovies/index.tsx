import * as React from 'react'
import { Preloader, Error, Red } from '..';
import { ApiFilmsWatchedResponse } from '../../../common/types';
import { Link } from 'react-router-dom';

type State = {
  loaded: boolean,
  error: boolean,
  films: ApiFilmsWatchedResponse
}

export default class WatchedMovies extends React.Component<{}, State> {
  public state: State = {
    loaded: false,
    error: false,
    films: []
  }

  public fetchContent = () => {
    window.fetch('/api/films/watched', {
      'credentials': 'same-origin'
    })
      .then((response): Promise<ApiFilmsWatchedResponse> => response.json())
      .then(films => {
        this.setState({
          loaded: true,
          error: false,
          films
        })
      })
      .catch(err => {
        this.setState({
          loaded: false,
          error: true
        })
        console.log(err)
      })
  }

  componentDidMount() {
    this.fetchContent()
  }

  render() {
    const { loaded, error, films } = this.state
    const filmList: React.ReactNode[] = []

    if (error) {
      return <Error text={<span><Red>А мы точно что-то смотрели?</Red><br />Кажется Острица всё забыла. Обратитесь к Акулёшке, с просьбами вернуть Острице память.</span>} />
    }

    if (!loaded) {
      return <Preloader />
    }

    for (let film of films) {
      if (!film.poster) {
        film.poster = '/img/poster_none.png'
      }
      const dateWatched = film.date_watched.split('-')
      let mm = dateWatched[1]
      if (mm.length < 2) {
        mm = `0${mm}`
      }
      const dateWatchedStr = `${parseInt(dateWatched[2])}.${mm}.${dateWatched[0]}`
      filmList.push(<div className='poll-answer' key={film.id}>
        <span style={{ color: '#cccccc', fontStyle: 'italic' }}>{dateWatchedStr}</span>
        <img alt='' src={film.poster} />
        <a
          href={`https://www.kinopoisk.ru/film/${film.kp_link}`}
          target='_blank'
          rel='noreferrer noopener'
        >{film.title}</a>, предложил{film.user.gender && 'а'}
        <Link
          to={`/users/${film.user.guid}`}
        >{film.user.name}</Link>
      </div>)
    }

    return <div style={{ textAlign: 'left' }}>
      <div style={{ float: 'right' }}>
        <Link to='/'>Голосование</Link> | <Link to='/films/added'>Предложенные фильмы</Link>
      </div>
      <h1>Чат уже посмотрел</h1>
      <div className='pollAnswers'>
        {filmList}
      </div>
    </div>
  }
}
