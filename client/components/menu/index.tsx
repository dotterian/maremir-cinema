import * as React from 'react'
import { NavLink } from 'react-router-dom';

const menuItems = [
  // { title: 'Кино', props: { to: '/', exact: true } },
  { title: 'Ссылки и активности', props: { to: '/' } },
  { title: 'Острица', props: { to: '/page/bot' } },
  { title: 'Пользователи', props: { to: '/users' } },
  { title: 'Ачивки', props: { to: '/achieve' } }
]

export default class Menu extends React.Component<{}> {
  public render() {
    const items = menuItems.map((item) => <li key={item.title}>
      <NavLink {...item.props}>{item.title}</NavLink>
    </li>)

    return <nav className='menu-container'>
      <ul className='menu'>
        {items}
      </ul>
    </nav>
  }
}
