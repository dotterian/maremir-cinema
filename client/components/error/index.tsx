import * as React from 'react'
import './error.scss'
import { randomArrayElement } from '../../../common/utils';

type PropTypes = {
  text: React.ReactNode
}

export default class Error extends React.Component<PropTypes> {
  static images: string[] = [
    'dunduk', 'fishfodder', 'frustration', 'huh', 'notimpressed', 'ollo1', 'ollo2', 'osoka', 'out', 'rules', 'sad', 'skeptic', 'tears', 'wat', 'wat2', 'where', 'why'
  ];

  public render() {
    const imagePath = randomArrayElement(Error.images)

    return <div className='center-all'>
      <h1>Этот сайт так не работает!</h1>
      <img alt='' src={`/img/${imagePath}.png`} />
      <p className='error'>{this.props.text}</p>
    </div>
  }
}
