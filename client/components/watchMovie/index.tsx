import * as React from 'react'
import { Link } from 'react-router-dom'

type State = {
  watching: boolean
}

export default class WatchMovie extends React.Component<{}, State> {
  public state = {
    watching: false
  }

  protected interval?: number

  public componentDidMount() {
    this.tryWatching()
    // Задал `window`, чтобы TypeScript брал правильную сигнатуру
    this.interval = window.setInterval(() => {
      this.tryWatching()
    }, 10000)
  }

  public componentWillUnmount() {
    if (this.interval) {
      window.clearInterval(this.interval)
    }
  }

  protected tryWatching = () => {
    window.fetch('/api/films/watching')
      .then((result): Promise<State> => result.json())
      .then(result => {
        this.setState({
          watching: result.watching
        })
      })
      .catch(() => {
        this.setState({
          watching: false
        })
      })
  }

  public render() {
    if (this.state.watching) {
      return <div className='watch-movie'>
        <h1>Прямо сейчас люди и рыбы смотрят кино!</h1>
        <Link className='btn' to='/watch'>Присоединиться!</Link>
      </div>
    }

    return ''
  }
}
