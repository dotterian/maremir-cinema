import * as React from 'react'
import { PollAnswer } from '..'
import { ApiFetchPollResult, ApiSavePollResult } from '../../../common/types';

type PropTypes = ApiFetchPollResult & {
  fetchPoll: () => void
}

type State = {
  checkboxes: string[],
  sending: boolean
}

export default class PollAnswers extends React.Component<PropTypes, State> {
  public state: State = {
    checkboxes: [],
    sending: false
  }

  sendVote = () => {
    this.setState({
      sending: true
    })
    window.fetch('/api/poll', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify({
        answers: this.state.checkboxes
      })
    })
      .then((response): Promise<ApiSavePollResult> => response.json())
      .then(result => {
        if (!result.saved) {
          window.alert('При сохранении произошла ошибка. Попробуйте очистить куки и проголосовать ещё раз.')
        }
        this.props.fetchPoll()
      })
  }

  discordLogin = () => {
    window.location.href = this.props.loginUrl
  }

  validateCheckboxes = (event: React.SyntheticEvent<HTMLInputElement>) => {
    const checkboxes = this.state.checkboxes
    const target = event.currentTarget
    const nodes = Array.from(document.querySelectorAll<HTMLInputElement>('input[name=pollAnswer]'))

    if (target.checked) {
      if (checkboxes.length === 2) {
        for (let node of nodes) {
          if (!node.checked) {
            node.disabled = true
          }
        }
      }
      checkboxes.push(target.value)
      this.setState({
        checkboxes
      })
    } else {
      const index = checkboxes.indexOf(target.value)
      if (index > -1) {
        checkboxes.splice(index, 1)
      }
      this.setState({
        checkboxes
      })
      for (let node of nodes) {
        if (node.disabled) {
          node.disabled = false
        }
      }
    }
  }

  public render() {
    const { voted, user, answers, nextDate, voteCount } = this.props
    const { checkboxes, sending } = this.state
    const films = []
    let button: React.ReactNode = ''

    if (!voted) {
      let disabled = true
      let buttonText = 'Голосовать!'
      if (checkboxes.length >= 1) {
        disabled = false
      }
      if (sending) {
        disabled = true
        buttonText = 'Голосуется…'
      }
      button = <button onClick={this.sendVote} disabled={disabled} className='btn'>{buttonText}</button>
    } else if (user.token === null) {
      button = <button onClick={this.discordLogin} className='btn'>Войти и проголосовать</button>
    }

    answers.sort((a, b) => {
      return (a.votes < b.votes) ? 1 : (a.votes > b.votes) ? -1 : 0
    })

    let counter = 0
    for (let answer of answers) {
      counter += 1
      films.push(<PollAnswer
        key={answer.id}
        onChange={this.validateCheckboxes}
        voted={voted}
        answer={answer}
        counter={counter}
      />)
    }

    return <div>
      <h1>Какой фильм будем смотреть {nextDate}?</h1>
      {(user.token !== null && !user.isMember) && <p style={{ color: 'red' }}>Вы должны быть членом «Маревого чата», чтобы иметь возможность голосовать</p>}
      {!voted &&
       <p>Привет, <b>{user.name}</b>! Выбери до трёх фильмов из списка.</p>
      }
      <div className='pollAnswers'>
        {films}
      </div>
      {voted &&
        <p>Всего проголосовали {voteCount} человек, рыб и прочей мари</p>
      }
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        {button}
      </div>
    </div>
  }
}
