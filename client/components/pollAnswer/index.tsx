import * as React from 'react'
import { Answer } from '../../../common/types';
import { declOfNum } from '../../../common/utils';
import './pollAnswer.scss'

type PropTypes = {
  voted: boolean,
  answer: Answer,
  onChange: React.EventHandler<React.SyntheticEvent<HTMLInputElement>>,
  counter: number
}

export default class PollAnswer extends React.Component<PropTypes> {
  public render() {
    const { voted, answer, onChange, counter } = this.props

    if (!answer.poster) {
      answer.poster = '/img/poster_none.png'
    }
    const votes = declOfNum(['голос', 'голоса', 'голосов'], answer.votes)

    return <div className='poll-answer'>
      {voted && <span className='counter'>{counter}</span>}
      <img alt='' src={answer.poster} />
      <a
        href={`https://www.kinopoisk.ru/film/${answer.kp_link}/`}
        target='_blank'
        rel='noreferrer nofollow'
      >{answer.title}</a>
      <span>{answer.votes} {votes}</span>
      {!voted && <div className='pretty p-icon p-square p-jelly'>
        <input type='checkbox' onChange={onChange} name='pollAnswer' value={answer.id} />
        <div className='state p-maremir'>
          <label />
        </div>
      </div>}
    </div>
  }
}
