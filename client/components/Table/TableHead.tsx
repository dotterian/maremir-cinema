import * as React from 'react'
import { Column, SortableFields } from './Table';

type PropTypes<Entity> = {
  column: Column<Entity>,
  order?: SortableFields<Entity>,
  asc?: boolean,
  onOrder?: (param: string) => void
}

export default class TableHead<Entity> extends React.Component<PropTypes<Entity>> {

  render() {
    const { column, order, asc, onOrder } = this.props
    if (column.sortable && column.attribute) {
      return <th
          className={(order === column.attribute) ? (asc) ? 'asc' : 'desc' : ''}
          onClick={() => onOrder(column.attribute)}
        >
          {column.title}
        </th>
    } else {
      return <th>{column.title}</th>
    }
  }
}
