import * as React from 'react'
import TableHead from './TableHead';
import TableRow from './TableRow';
import './table.scss'

type Base = {
  [key: string]: any
}

export type Column<Entity extends Base> = {
  title: string,
  attribute: Extract<keyof Entity, string>,
  sortable: boolean,
  render?: (key: number, entity: Entity) => React.ReactNode
}

export type SortableFields<Entity extends {
  [key: string]: any
}> = Extract<{
  [K in keyof Entity]: Entity[K] extends string | number ? K : never
}[keyof Entity], string>

export type TableProps<Entity extends Base> = {
  columns: Column<Entity>[],
  data: Entity[],
  order: SortableFields<Entity>,
  asc?: boolean,
  secondarySort: SortableFields<Entity>
  filter: boolean,
  filterParam: Extract<keyof Entity, string>,
  filterPlaceholder: string,
  onClick?: (entity: Entity) => void
}

type StateTypes<Entity> = {
  filter: string,
  order: SortableFields<Entity>,
  asc: boolean
}

export default class Table<Entity extends Base> extends React.Component<TableProps<Entity>, StateTypes<Entity>> {

  public state:StateTypes<Entity> = {
    filter: '',
    order: this.props.order,
    asc: this.props.asc ? true : false
  }

  sortable = (order: any): order is SortableFields<Entity> => {
    return ['string', 'number'].includes(typeof this.props.data[0][order])
  }

  order = (order: string) => {
    console.log(order)
    if (!this.sortable(order)) {
      console.log('fail')
      return
    }
    var asc = this.props.asc ? true : false
    if (this.state.order === order) {
      asc = !this.state.asc
    }
    this.setState({
      order,
      asc
    })
  }

  filter = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({
      filter: e.currentTarget.value
    })
  }

  public render() {
    const { columns, data, onClick, filterParam, secondarySort } = this.props
    const { filter, order, asc } = this.state

    const head: React.ReactNode[] = []
    const rows: React.ReactNode[] = []

    for (let column of columns) {
      const props: {
        key: string,
        column: Column<Entity>,
        order?: SortableFields<Entity>,
        asc?: boolean,
        onOrder?: (param: string) => void,
      } = {
        key: `${column.title}_th`,
        column
      }
      if (column.sortable) {
        props.order = order
        props.asc = asc
        props.onOrder = this.order
      }
      head.push(<TableHead<Entity> {...props} />)
    }

    data.sort((a, b) => {
      let aVal: string | number = a[order]
      let bVal: string | number = b[order]
      const secondary =
      (secondarySort && order !== secondarySort) ?
      (a[secondarySort] < b[secondarySort]) ? -1 : 1
      : 0
      const sort = (aVal < bVal) ? 1
        : (aVal > bVal) ? -1
        : secondary
      return (asc) ? -sort : sort
    })

    let key = 0
    for (let dataRow of data) {
      key += 1
      if (filter) {
        const re = new RegExp(filter, 'i')
        if (!re.test(dataRow[filterParam])) {
          continue;
        }
      }
      const props = {
        columns,
        data: dataRow,
        onClick,
        key,
        rowNum: key
      }
      rows.push(<TableRow<Entity> {...props} />)
    }

    return <React.Fragment>
      { this.props.filter && <input
        className='mm-table-filter'
        placeholder={this.props.filterPlaceholder}
        onInput={this.filter}
      /> }
      <table className='mm-table'>
        <thead>
          <tr>
            {head}
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    </React.Fragment>
  }

}
