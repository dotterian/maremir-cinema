import * as React from 'react'
import { Column } from './Table';

type PropTypes<Entity> = {
  rowNum: number,
  columns: Column<Entity>[],
  data: Entity,
  onClick: null | ((entity: Entity) => void)
}

export default class TableRow<Entity> extends React.Component<PropTypes<Entity>> {

  onclick = (ev: React.SyntheticEvent<HTMLTableRowElement>) => {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick(this.props.data)
    }
  }

  render() {
    const { columns, data } = this.props
    const tds: React.ReactNode[] = []

    let key = 0
    for (let column of columns) {
      key += 1
      if (data.hasOwnProperty(column.attribute)) {
        let content: React.ReactNode = data[column.attribute]
        if (typeof column.render === 'function') {
          content = column.render(this.props.rowNum, data)
        }
        tds.push(<td key={`${this.props.rowNum}_${column.attribute}_${key}`}>
          {content}
        </td>)
      }
    }

    return <tr
      onClick={this.onclick}
    >
      {tds}
    </tr>
  }
}
