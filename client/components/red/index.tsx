import * as React from 'react'

export default class Red extends React.Component<{}> {
  public render() {
   return <span style={{ color: '#FF0000' }}>{this.props.children}</span>
  }
}
