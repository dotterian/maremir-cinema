// @flow

import * as React from 'react'
import { view } from 'react-easy-state'
import state from '../../state'

@view
export default class Popup extends React.Component<{}> {
  render() {
    if (!state.popup) {
      return ''
    }
    return <div
      className='lightbox-container'
      onClick={() => {
        state.popup = false
      }}
    >
      <div className='popup-container'>
        {state.popupChildren}
      </div>
    </div>
  }
}
