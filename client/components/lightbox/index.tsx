import * as React from 'react'
import { view } from 'react-easy-state'
import state from '../../state'

@view
export default class Lightbox extends React.Component<{}> {
  render () {
    if (!state.lightbox) {
      return ''
    }
    return <div
      className='lightbox-container'
      onClick={() => {
        state.lightbox = false
      }}
    >
      <img alt='' src={state.lightboxImage} />
    </div>
  }
}
