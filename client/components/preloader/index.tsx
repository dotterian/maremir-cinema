import * as React from 'react'

export default class Preloader extends React.Component<{}> {
  public render() {
    return <div className='preloader' />
  }
}
