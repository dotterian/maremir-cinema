import * as React from 'react'
import { Preloader, Error, Red } from '..';
import { ApiFilmsAddedResponse } from '../../../common/types';
import { Link } from 'react-router-dom';

type State = {
  loaded: boolean,
  error: boolean,
  films: ApiFilmsAddedResponse
}

export default class AddedMovies extends React.Component<{}, State> {
  public state: State = {
    loaded: false,
    error: false,
    films: []
  }

  fetchContent = () => {
    window.fetch('/api/films/added', {
      credentials: 'same-origin'
    })
      .then((response): Promise<ApiFilmsAddedResponse> => response.json())
      .then(films => {
        this.setState({
          loaded: true,
          error: false,
          films
        })
      })
      .catch(err => {
        this.setState({
          loaded: false,
          error: true
        })
        console.log(err)
      })
  }

  componentDidMount() {
    this.fetchContent()
  }

  render() {
    const { loaded, error, films } = this.state
    const filmList: React.ReactNode[] = []

    if (error) {
      return <Error text={<span>
        <Red>Увы и ах, мы потерпели крах.</Red><br />
        Если Акулёшка ещё не чинит данную поломку, то он однозначно должен!
      </span>} />
    }

    if (!loaded) {
      return <Preloader />
    }

    for (let film of films) {
      if (!film.poster) {
        film.poster = '/img/poster_none.png'
      }
      filmList.push(<div className='poll-answer' key={film.id}>
        <img alt='' src={film.poster} />
        <a
          href={`https://www.kinopoisk.ru/film/${film.kp_link}/`}
          target='_blank'
          rel='noreferrer noopener'
        >{film.title}</a>
      </div>)
    }

    return <div style={{ textAlign: 'left' }}>
      <div style={{ float: 'right' }}>
        <Link to='/'>Голосование</Link> | <Link to='/films/watched'>Просмотренные фильмы</Link>
      </div>
      <h1>Добавленные фильмы</h1>
      <div className='pollAnswers'>
        {filmList}
      </div>
    </div>
  }
}
