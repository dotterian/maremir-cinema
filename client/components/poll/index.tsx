import * as React from 'react'
import { Link, Switch, Route } from 'react-router-dom'
import { Red, Error, Preloader, PollAnswers, AddedMovies, WatchedMovies } from '..';
import { ApiFetchPollResult } from '../../../common/types';

type State = ApiFetchPollResult & {
  loaded: boolean,
  error: boolean
}

export default class Poll extends React.Component<{}, State> {

  public state: State = {
    loaded: false,
    error: false,
    pollActive: false,
    voted: false,
    voteCount: 0,
    nextDate: '',
    loginUrl: '',
    answers: [],
    votedIds: [],
    pollResults: {
      today: false,
    },
    user: {
      id: '',
      name: '',
      token: '',
      isMember: false
    }
  }

  protected fetchPoll = () => {
    window.fetch('/api/poll', {
      credentials: 'same-origin'
    })
      .then((response): Promise<ApiFetchPollResult> => response.json())
      .then(result => {
        this.setState({
          loaded: true,
          error: false,
          ...result
        })
      })
      .catch(err => {
        this.setState({
          loaded: true,
          error: true
        })
        console.error(err)
      })
  }

  public componentDidMount() {
    this.fetchPoll()
  }

  public render() {
    const { error, loaded, pollActive, pollResults } = this.state

    if (error) {
      return <Error
        text={<span>
          <Red>Ну, Акулёшка! Ну, злодей!</Red><br />
          Даже голосовалку и ту сломал!<br />
          Дайте ему <Red>леща</Red>, если увидите.
        </span>}
      />
    }
    if (!loaded) {
      return <Preloader />
    }

    let pollContent = <div>
      <h1>Следующее голосование за фильм начнётся в субботу</h1>
      <p>Пока можете накидывать свои фильмы Острице.</p>
    </div>

    if (pollActive) {
      pollContent = <PollAnswers
        {...this.state}
        fetchPoll={this.fetchPoll}
      />
    }

    let watchString = 'В прошлый раз чат смотрел'

    if (pollResults.today) {
      watchString = 'Сегодня смотрим'
    }

    return <div className='poll'>
      <h1>
        {watchString} <a
          href={`https://www.kinopoisk.ru/film/${pollResults.kp_link}/`}
          target='_blank'
          rel='noreferrer noopener'
        >{pollResults.title}</a> по рекомендации <Link
          to={`/users/${pollResults.user.guid}`}
        >{pollResults.user.name}</Link>
      </h1>
      <hr/>
      <Switch>
        <Route exact path='/' render={() => <div>
          <div style={{ float: 'right' }}>
            <Link to='/films/watched'>
              Просмотренные фильмы
            </Link> | <Link to='/films/added'>
              Предложенные фильмы
            </Link>
          </div>
          {pollContent}
        </div>} />
        <Route exact path='/films/added' component={AddedMovies} />
        <Route exact path='/films/watched' component={WatchedMovies} />
      </Switch>
    </div>
  }
}
