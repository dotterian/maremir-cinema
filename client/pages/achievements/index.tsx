import * as React from 'react'
import { ApiAchievements } from '../../../common/types'
import { Achievement } from '../../../common/types/models/Achievement'
import state from '../../state'
import { Link, RouteComponentProps } from 'react-router-dom';
import { Preloader, Error, Red } from '../../components';
import Table, { TableProps } from '../../components/Table/Table';

type State = {
  loading: boolean,
  error: boolean,
  info?: ApiAchievements,
}

export default class AchievementsPage extends React.Component<RouteComponentProps, State> {

  public state: State = {
    loading: true,
    error: false
  }

  componentDidMount() {
    window.fetch(`/api/achievements`, {
      credentials: 'same-origin'
    })
      .then((response):Promise<ApiAchievements> => response.json())
      .then(info => {
        this.setState({
          loading: false,
          info
        })
      })
      .catch(err => {
        this.setState({
          loading: false,
          error: true
        })
        console.error(err)
      })
  }

  showUsers = (items: Achievement[]) => {
    const rows = []

    for (let item of items) {
      rows.push(<div className='achievement' key={item.id}>
        <img
          onClick={() => {
            state.lightboxImage = `https://maremir.dotterian.ru/achievements/${item.id}.png`
          }}
          alt={item.name}
          title={`${item.name}\n${item.text}`}
          src={`https://maremir.dotterian.ru/achievements/${item.id}.png`}
          style={{ width: '500px', cursor: 'pointer' }}
        />
        {item.User && <Link to={`/users/${item.User.guid}`}>{item.User.name}</Link>}
      </div>)
    }

    if (items.length > 1) {
      state.popupChildren = <div>
        <div className='achievements-container'>
          {rows}
        </div>
      </div>
    } else {
      state.popupChildren = rows
    }
    state.popup = true
  }

  render() {
    if (this.state.error) {
      return <Error
        text={<span><Red>Да сколько можно-то?</Red><br />Доколе Акулёшка будет всё ломать?<br />Пора прекратить этот произвол!<br />Акулёшку <Red>НА НОЖ!</Red></span>} />
    }

    if (this.state.loading) {
      return <Preloader />
    }

    const latest = []
    const { last, full } = this.state.info
    let TBL: React.ReactNode = ''

    if (last) {
      for (let item of last) {
        latest.push(<div key={item.id}>
          <img
            onClick={() => {
              state.lightboxImage = `http://maremir.dotterian.ru/achievements/${item.id}.png`
              state.lightbox = true
            }}
            alt={item.name}
            title={`${item.name}\n${item.text}`}
            src={`http://maremir.dotterian.ru/achievements/${item.id}.png`}
            style={{ width: '500px', cursor: 'pointer' }}
          />
          {item.givenBy && <Link to={`/users/${item.givenBy.guid}`}>{item.givenBy.name}</Link>}
          {item.givenBy && item.User && ' ▶ '}
          {item.User && <Link to={`/users/${item.User.guid}`}>{item.User.name}</Link>}
        </div>)
      }
    }

    if (full) {
      const props: TableProps<ApiAchievements['full'][0]> = {
        columns: [
          {
            title: 'Название ачивки',
            attribute: 'title',
            sortable: true
          },
          {
            title: 'Выдано шт.',
            attribute: 'length',
            sortable: true
          }
        ],
        data: full,
        order: 'length',
        asc: false,
        secondarySort: 'title',
        filter: true,
        filterParam: 'title',
        filterPlaceholder: 'Поиск по названию',
        onClick: (entity: ApiAchievements['full'][0]) => {
          this.showUsers(entity.items)
        }
      }
      TBL = <Table<ApiAchievements['full'][0]> {...props} />
    }

    return <div>
      <h1>Ачивки</h1>
      <h2>Последние ачивки</h2>
      <div className='achievements-container'>
        {latest}
      </div>
      <h2>Полный список ачивок</h2>
      {TBL}
    </div>
  }

}
