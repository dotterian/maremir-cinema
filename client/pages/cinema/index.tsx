import * as React from 'react'
import FilePlayer from 'react-player/lib/players/FilePlayer'
import { RouteComponentProps } from 'react-router';
import socketIOClient from 'socket.io-client'
import './cinema.scss'
import { Link } from 'react-router-dom';
import {declOfNum} from "../../../common/utils";

const host = process.env.SERVER_NAME || 'localhost:1488'

interface IStateTypes {
  userCount: number
}

export default class Cinema extends React.Component<RouteComponentProps<{
  stream?: string
}>, IStateTypes> {

  public state = {
    userCount: 0
  }

  protected io: SocketIOClient.Socket = null

  componentWillMount(): void {
    console.log(socketIOClient)
    this.io = socketIOClient(`${host}`)
    this.io.on('updateUserCounter', (userCount) => {
      this.setState({
        userCount
      })
    })
  }

  render() {
    const { userCount } = this.state
    const ord = declOfNum(['зритель', 'зрителя', 'зрителей'])
    return <div className='Cinema'>
      <div className='vjs-player'>
        <FilePlayer
          playing
          controls
          url={`https://cinema.dotterian.ru/hls/${this.props.match.params.stream || 'cinema'}.m3u8`}
          width='100%'
          height='100%'
        />
      </div>
      <Link to='/' className='back-button'>&lt;</Link>
      <div className='current-users'>
        {`${userCount} ${ord(userCount)}`}
      </div>
    </div>
  }
}
