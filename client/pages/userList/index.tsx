import * as React from 'react'
import { Error, Preloader, Red } from '../../components'
import { RouteComponentProps } from 'react-router';
import { ApiUsersResponse } from '../../../common/types';
import Table, { TableProps } from '../../components/Table/Table';

type Sortable = 'name' | 'words_total' | 'words_today' | 'sudak_counter' | 'achievements'

type State = {
  loading: boolean,
  filter: string,
  order: Sortable,
  asc: boolean,
  error: boolean,
  info?: ApiUsersResponse
}

export default class Users extends React.Component<RouteComponentProps, State> {

  public state: State = {
    loading: true,
    filter: '',
    order: 'words_today',
    asc: false,
    error: false
  }

  componentDidMount() {
    window.fetch('/api/users', {
      credentials: 'same-origin'
    })
      .then((response):Promise<ApiUsersResponse> => response.json())
      .then(info => {
        this.setState({
          loading: false,
          error: false,
          info
        })
      })
      .catch(err => {
        this.setState({
          loading: false,
          error: true
        })
        console.log(err)
      })
  }

  go = (guid: string) => {
    this.props.history.push(`/users/${guid}`)
  }

  order = (order: Sortable) => {
    let asc = false
    if (this.state.order === order) {
      asc = !this.state.asc
    }
    this.setState({
      order,
      asc
    })
  }

  render() {
    const { loading, error, info } = this.state

    if (error) {
      return <Error
        text={<span><Red>Да сколько можно-то?</Red><br />Доколе Акулёшка будет всё ломать?<br />Пора прекратить этот произвол!<br />Акулёшку <Red>НА НОЖ!</Red></span>} />
    }

    if (loading) {
      return <Preloader />
    }

    let TBL:React.ReactNode = ''

    if (info) {
      const props: TableProps<ApiUsersResponse[0]> = {
        columns: [
          {
            title: '',
            attribute: 'name',
            sortable: false,
            render: (key: number) => {
              return key
            }
          },
          {
            title: 'Имя',
            attribute: 'name',
            sortable: true
          },
          {
            title: 'Слов сегодня',
            attribute: 'words_today',
            sortable: true
          },
          {
            title: 'Слов за всё время',
            attribute: 'words_total',
            sortable: true
          },
          {
            title: 'Раз судаком',
            attribute: 'sudak_counter',
            sortable: true
          }
        ],
        data: info,
        order: 'words_today',
        asc: false,
        secondarySort: 'name',
        filter: true,
        filterParam: 'name',
        filterPlaceholder: 'Найди себя! Или не себя!',
        onClick: (entity: ApiUsersResponse[0]) => {
          this.go(entity.guid)
        }
      }
      TBL = <Table<ApiUsersResponse[0]> {...props} />
    }

    return <div>
      <h1>Пользователи</h1>
      {TBL}
    </div>
  }
}
