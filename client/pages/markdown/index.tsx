import * as React from 'react'
import { RouteComponentProps } from 'react-router';
import { Error, Red, Preloader } from '../../components';
import { ApiPageResponse } from '../../../common/types';

type PropTypes = RouteComponentProps & {
  match: {
    params: {
      pageName: string
    }
  }
}

type State = {
  loaded: boolean,
  error: boolean,
  text: string
}

export default class Markdown extends React.Component<PropTypes, State> {

  public state: State = {
    loaded: false,
    error: false,
    text: ''
  }


  loadPage = (props: PropTypes) => {
    window.fetch(`/api/page/${props.match.params.pageName}`, {
      credentials: 'same-origin'
    })
      .then((response): Promise<ApiPageResponse> => response.json())
      .then(({ text }) => {
        this.setState({
          loaded: true,
          error: false,
          text
        })
      })
      .catch(err => {
        this.setState({
          loaded: false,
          error: true
        })
        console.log(err)
      })
  }

  componentDidMount() {
    this.loadPage(this.props)
  }

  componentWillReceiveProps(nextProps: PropTypes) {
    this.setState({
      loaded: false,
      error: false
    })
    this.loadPage(nextProps)
  }

  render() {
    const { loaded, error, text } = this.state

    if (error) {
      return <Error
        text={<span><Red>При загрузке страницы случилась ошибка.</Red><br />Скажите Акулёшке, что эта страница не загрузилась.</span>} />
    }

    if (!loaded) {
      return <Preloader />
    }

    return <div className='markdown' dangerouslySetInnerHTML={{__html: text}} />
  }
}
