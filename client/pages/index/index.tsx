import * as React from 'react'
import { RouteComponentProps } from 'react-router';
import { WatchMovie, Poll } from '../../components'

export default class Page extends React.Component<RouteComponentProps> {
  public render() {
    return <div className='page-index'>
      <WatchMovie />
      <Poll />
    </div>
  }
}
