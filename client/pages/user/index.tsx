import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { Error, Red, Preloader } from '../../components'
import { Link } from 'react-router-dom'
import { ApiUserResponse, ApiAchievementDelete } from '../../../common/types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons/faTrashAlt'
import state from '../../state'

type RouteProps = {
  guid: string
}

type State = {
  guid: string,
  loading: boolean,
  error: boolean,
  info?: ApiUserResponse
}

export default class UserPage extends React.Component<RouteComponentProps<RouteProps>, State> {

  public state: State = {
    guid: '',
    loading: true,
    error: false,
  }

  public loadInfo = (guid: string) => {
    this.setState({
      guid,
      loading: true,
      error: false
    })
    window.fetch(`/api/users/${guid}`, {
      credentials: 'same-origin'
    })
      .then((response):Promise<ApiUserResponse> => response.json())
      .then(info => {
        this.setState({
          loading: false,
          error: false,
          info
        })
      })
      .catch(err => {
        this.setState({
          loading: false,
          error: true
        })
        console.log(err)
      })
  }

  componentDidUpdate() {
    const { guid } = this.props.match.params
    if (guid !== this.state.guid) {
      this.loadInfo(guid)
    }
  }

  componentDidMount() {
    this.loadInfo(this.props.match.params.guid)
  }

  discordLogin = () => {
    const { guid, info } = this.state
    if (guid) {
      window.localStorage.setItem('mm_redirectAfterLogin', `/users/${guid}`)
      if (info.loginUrl) {
        window.location.href = info.loginUrl
      }
    }
  }

  deleteAchievement = (id: number) => {
    if (!window.confirm('Вы дествительно хотите удалить эту ачивку?')) {
      return
    }
    window.fetch(`/api/achievements/${id}`, {
      method: 'DELETE',
      credentials: 'same-origin'
    })
      .then((response):Promise<ApiAchievementDelete> => response.json())
      .then(result => {
        this.setState({
          info: {
            ...this.state.info,
            achievements: result.achievements
          }
        })
      })
      .catch(err => {
        window.alert('При удалении произошла ошибка')
        console.log(err)
      })
  }

  public render() {
    const { error, loading, info } = this.state

    if (error) {
      return <Error
        text={<span><Red>Да сколько можно-то?</Red><br />
          Доколе Акулёшка будет всё ломать?<br />Пора прекратить этот произвол!<br />Акулёшку <Red>НА НОЖ!</Red>
        </span>}
      />
    }

    if (loading) {
      return <Preloader />
    }

    const achievements = []
    let login: React.ReactNode = ''

    if (info.achievements) {
      info.achievements = info.achievements.sort((a, b) => {
        return (a.id > b.id) ? -1 : (a.id < b.id) ? 1 : 0
      })
      for (let item of info.achievements) {
        achievements.push(<div key={item.id} style={{ position: 'relative' }}>
          <img
            onClick={() => {
              state.lightboxImage = `https://maremir.dotterian.ru/achievements/${item.id}.png`
              state.lightbox = true
            }}
            alt={item.name}
            title={`${item.name}\n${item.text}`}
            src={`https://maremir.dotterian.ru/achievements/${item.id}.png`}
            style={{ width: '500px', cursor: 'pointer' }}
          />
          <span
            style={{ fontStyle: 'italic', color: '#ccc' }}
          >{item.given_at}</span> <Link to={`/users/${item.givenBy.guid}`}>{item.givenBy.name}</Link> {info.canDelete && <button onClick={() => this.deleteAchievement(item.id)} className='btn btn-danger remove-attachment' style={{
            float: 'right'
          }}><FontAwesomeIcon icon={faTrashAlt} /></button>}
        </div>)
      }
    }

    if (!info.loggedIn) {
      login = [
        <button key='btn' onClick={this.discordLogin} className='btn'>Войди, чтобы иметь возможность редактировать эту страницу</button>,
        <hr key='hr' />
      ]
    }

    return <div>
      <h1>Страница пользователя {info.name}</h1>
      {login}
      <h2>Ачивки</h2>
      <div className='achievements-container'>
        {achievements}
      </div>
    </div>
  }

}
