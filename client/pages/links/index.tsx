import * as React from 'react'
import { Link, RouteComponentProps } from 'react-router-dom'
import './style.scss'

export default class Links extends React.Component<RouteComponentProps> {
  render() {
    return <div className='links-page'>
      <h1>Активности чата</h1>
      <h2>Каналья Зелёная</h2>
      <p>В канале с вышеобозначенным названием некий Зелёный проводит ролёвки, играет в «Свою Игру» и в целом старается развлекать честной народ своими силами (а сил у него много, уж поверьте).</p>
      <p>Актуальную информацию о развлечениях уточняйте непосредственно у Зелёного.</p>
      {/*<h2>Кино</h2>
      <p>По средам <b>в 21:00 по Москве</b> мы смотрим кино, выбранное общим голосованием.</p>
      <div className='links-grid'>
        <Link to='/'>
          <img alt='' src='/img/poll.svg' />
          <span>Голосование за кино</span>
        </Link>
        <Link to='/page/bot'>
          <img alt='' src='/img/link.svg' />
          <span>Инструкция по добавлению фильмов при помощи Острицы</span>
        </Link>
      </div>*/}
      {/*<h2>Настольные игры</h2>
      <p>Каждую пятницу, приблизительно вечером по Москве, мы играем в настольные игры при помощи Tabletop Simulator</p>
      <div className='links-grid'>
        <a
          href='http://store.steampowered.com/app/286160/Tabletop_Simulator/'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/steam.svg' />
          <span>Страница игры в Steam</span>
        </a>
        <a
          href='https://steamcommunity.com/sharedfiles/filedetails/?id=1433260378'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/steam.svg' />
          <span>Список периодически играемых настольных игр</span>
        </a>
        <a
          href='https://docs.google.com/document/d/1fqSiqYYBkEi-MOBmeggDpk6d9AACh3FejgOlTigf7kw/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Список периодически играющих постояльцев чата</span>
        </a>
      </div>
      <div className='alert-info'><b>Внимание!</b> Игру нужно обязательно приобрести через Steam.</div>*/}
      <h2>Радио</h2>
      <p>В соответствующем голосовом канале работает «Маревое радио».</p>
      <p>С инструкцией по запуску вы можете ознакомиться в закреплённых сообщениях в канале <b>#радиобот</b></p>
      <p>Так же, добавлять песни можно через web-интерфейс (требует авторизации через Discord и вашего присутствия в
        голосовом канале с радиоботом)</p>
      <div className='links-grid'>
        <a
          target='_blank'
          href='https://web.rythmbot.co/guilds/416670606356905984'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/link.svg' />
          <span>Web-интерфейс радиобота</span>
        </a>
      </div>
      <h2>Dwarf Fortress</h2>
      <p>Когда-то мы играли в Dwarf Fortress, по принципу переходящей власти. Каждый правитель правил один гномий год.</p>
      <div className='alert-info'>
        <b>ВАЖНО!</b>Даже если вы не умеете играть в Dwarf Fortress, вы можете присоединиться к нам в любой момент,
        записавшись в очередь правителей, и повергнуть нашу крепость в хаос.<br />
        Помните, главный девиз Dwarf Fortress: <b>Проигрывать — весело!</b><br/>
        Для того, чтобы присоединиться напишите Принцессе АкуЛёшке.
      </div>
      <div className='links-grid'>
        <a
          href='https://docs.google.com/document/d/1TtT6DL6Np_TQdN8PKiwM2FzzFYFUgzdFXdosfd2QyUM/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/word.svg' />
          <span>Летопись крепости</span>
        </a>
        <a
          href='https://docs.google.com/spreadsheets/d/1MfCIV9ZYlomzjHkC8NuZ3eah_RKV82w7qdfKlJsiVEo/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Список правителей крепости</span>
        </a>
        <a
          href='http://dfwk.ru'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/wiki.svg' />
          <span>Русскозычная вики по игре</span>
        </a>
        <a
          href='https://drive.google.com/drive/folders/1LlxE1QhtGYOe1XepojS_OTbRIeC3wNo8?usp=sharing'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/folder.svg' />
          <span>Архивы сборок игры от АкуЛёшки</span>
        </a>
        <a
          href='https://drive.google.com/drive/folders/1AM5z35H2yspaf8-nSFjb5cmm2-Ky97Jx'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/folder.svg' />
          <span>Ежегодные сохранёнки</span>
        </a>
      </div>
      <h2>Слепой телефон</h2>
      <p>У нас их целых два!</p>
      <p>Первый — для тех, кто не сильно заморачивается над качеством и имеет устройство с поддержкой Flash.</p>
      <p>Второй проводится непосредственно в чате. Поучаствовать в нём вы можете записавшись в соответствующей
        табличке</p>
      <div className='links-grid'>
        <a
          target='_blank'
          href='http://doodle.multator.ru/room/maremir'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/link.svg' />
          <span>Простой «Слепой телефон» на Flash</span>
        </a>
        <a
          href='https://docs.google.com/spreadsheets/d/1XLemT-_kjx8mv6v2LXCrGl1eHTo_7Z4PeAT62Qoknz0/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Табличка участников «сложного» Слепого Телефона</span>
        </a>
      </div>
      <h2>Пейринги</h2>
      <p>Если вы считаете, что вы с другим пользователем чата составляете пейринг, вы можете записаться в официальную
        таблицу пейрингов чата</p>
      <div className='links-grid'>
        <a
          target='_blank'
          href='https://docs.google.com/forms/d/e/1FAIpQLSebqiwsw2NkLg_lPfo8xXLAYGB6OyEbN4lY80Ag7fj0_gXcmw/viewform'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/poll.svg' />
          <span>Форма регистрации пейринга</span>
        </a>
        <a
          target='_blank'
          href='https://docs.google.com/spreadsheets/d/1rqF_GL_xYTMLtdGAM20AfGoEUErclBrtqbFsSwmtjMo/edit'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Таблица пейрингов</span>
        </a>
      </div>
      <h2>Прочие ссылки</h2>
      <div className='links-grid'>
        <a
          target='_blank'
          href='http://ru.mare.wikia.com'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/wiki.svg' />
          <span>Маревая Викия</span>
        </a>
        <a
          target='_blank'
          href='https://docs.google.com/document/d/17H9rtN7pF3bsWuMpmrT6_ENfgcg9KYFhioudLv6GLLo/edit'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/word.svg' />
          <span>Краткий гайд по ролям чата</span>
        </a>
        <a
          href='https://docs.google.com/spreadsheets/d/1k2o5D5TxbMowaHmeV7An1LmKXEkVKxIrH2jwL09q1So/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Рыбное творчество и стена шиппинга</span>
        </a>
        <a
          href='https://docs.google.com/spreadsheets/d/1OERoysH4q72WmeNf_x0W2Ykk9JJIsw3jWuxed3qwYJQ/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Раздача игр в добрые руки</span>
        </a>
        <a
          href='https://docs.google.com/spreadsheets/d/1FjGe2ys3XxAQtssQCb6U6AORrm8wjAUT2KTxjziwsEs/edit'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img alt='' src='/img/excel.svg' />
          <span>Кто где живёт (для совместных встреч вне чата)</span>
        </a>
      </div>
    </div>
  }
}
