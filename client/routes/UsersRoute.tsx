import * as React from 'react'
import { Switch } from 'react-router';
import DefaultLayout from '../layouts/DefaultLayout';
import { Error, Red } from '../components';
import { UserListPage, UserPage } from '../pages';

export default class UserRoute extends React.Component<{}> {
  render() {
    return <Switch>
      <DefaultLayout exact path='/users' component={UserListPage} />
      <DefaultLayout path='/users/:guid' component={UserPage} />
      <DefaultLayout path='*' exact component={() => {
        return <Error
          text={<span><Red>Такой страницы нет. Её съели рыбы.</Red><br />Либо, Акулёшка опять что-то сломал, либо с вами что-то не так.</span>} />
      }} />
    </Switch>
  }
}
