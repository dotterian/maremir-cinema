import * as React from 'react'
import { Switch, Route } from 'react-router';
import { CinemaPage } from '../pages';
import DefaultLayout from '../layouts/DefaultLayout';
import { Error, Red } from '../components';

export default class CinemaRoute extends React.Component<{}> {
  render() {
    return <Switch>
      <Route exact path='/watch' component={CinemaPage} />
      <Route path='/watch/:stream' component={CinemaPage} />
      <DefaultLayout path='*' exact component={() => {
        return <Error
          text={<span><Red>Такой страницы нет. Её съели рыбы.</Red><br />Либо, Акулёшка опять что-то сломал, либо с вами что-то не так.</span>} />
      }} />
    </Switch>
  }
}
