import * as React from 'react'
import DefaultLayout from '../layouts/DefaultLayout';
import { Error, Red } from '../components';
import { Switch } from 'react-router';
import { LinksPage, MarkdownPage } from '../pages';

export default class PageRoute extends React.Component<{}> {
  render() {
    return <Switch>
      <DefaultLayout path='/page/links' component={LinksPage} />
      <DefaultLayout path='/page/:pageName' component={MarkdownPage} />
      <DefaultLayout path='*' exact component={() => {
        return <Error
          text={<span><Red>Такой страницы нет. Её съели рыбы.</Red><br />Либо, Акулёшка опять что-то сломал, либо с вами что-то не так.</span>} />
      }} />
    </Switch>
  }
}
