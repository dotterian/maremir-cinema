import * as React from 'react'
import { render } from 'react-dom'
import App from './app'

clearTimeout(window['loaderTimeout'])
if (window['bubbleInterval']) {
  clearInterval(window['bubbleInterval'])
  const appLoader = document.getElementById("app-loader")
  appLoader.classList.add('out')
  setTimeout(() => {
    render(<App />, document.getElementById("marechat"))
  }, 1000)
} else {
  render(<App />, document.getElementById("marechat"))
}
