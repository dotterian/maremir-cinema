import * as React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import DefaultLayout from './layouts/DefaultLayout'
import {IndexPage, AchievementsPage, LinksPage} from './pages'
import { Error, Red, Lightbox, Popup } from './components'
import { PageRoute, UsersRoute, CinemaRoute } from './routes';
import './index.scss'
import state from './state'
import { view } from 'react-easy-state';

@view
export default class Application extends React.Component<{}, {}> {
  public render() {
    return <Router>
      <React.Fragment>
        <Switch>
          <DefaultLayout exact path='/' component={LinksPage} />
          {/*<DefaultLayout path='/films' component={IndexPage} />*/}
          <DefaultLayout exact path='/achieve' component={AchievementsPage} />
          <Route path='/page' component={PageRoute} />
          <Route path='/users' component={UsersRoute} />
          <Route path='/watch' component={CinemaRoute} />
          <DefaultLayout path='*' exact component={() => <Error text={<span>
            <Red>
              Такой страницы нет. Её съели рыбы.
            </Red>
            <br/>
            Либо Акулёшка опять что-то сломал, либо с вами что-то не так.
          </span>}/>} />
        </Switch>
        {state.lightbox && <Lightbox />}
        {state.popup && <Popup />}
      </React.Fragment>
    </Router>
  }
}
