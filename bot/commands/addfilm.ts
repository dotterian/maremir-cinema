import { Message, RichEmbed } from 'discord.js';
import { getManager } from 'typeorm';
import * as og from 'open-graph-scraper'
import Film from '../../common/models/Film';
import he = require('he');

export default async ({ content, guild, channel, member }: Message) => {
  const re = /(https:\/\/www\.kinopoisk\.ru\/film\/([^ \n/]+))/
  const url = re.exec(content)
  const admin = guild.members.find('id', '284746199918051330')
  if (url === null) {
    channel.send('Увы, я не вижу ссылки на Кинопоиск.')
    return
  }
  const manager = getManager()
  let film = await manager.findOne(Film, {
    kp_link: url[2]
  }, {
    relations: ['user']
  })
  if (film) {
    if (film.user_uuid === member.id) {
      channel.send(`Но ты уже предлагал${film.user.gender !== true ? 'а' : ''} этот фильм!`)
    } else {
      const member2 = guild.members.find('id', film.user_uuid)
      channel.send(`Извини, ${member}, но этот фильм уже предложил${film.user.gender !== true ? 'а' : ''} ${member2}.`)
    }
    return
  }
  og({
    url: url[1],
    charset: 'win1251'
  }, async (err, meta) => {
    try {
      if (err !== false) {
        throw err
      }
      const data = meta.data
      data.ogTitle = he.decode(data.ogTitle)
      film = manager.create(Film, {
        title: data.ogTitle,
        user_uuid: member.id,
        kp_link: url[2],
        watched: false,
        date_watched: null,
        poster: data.ogImage.url
      })
      await manager.save(film)
      const message = new RichEmbed({
        title: `${member.displayName} добавил(а) фильм ${data.ogTitle} в очередь просмотра`
      })
      message.setColor('RANDOM')
      message.setImage(data.ogImage.url)
      channel.send(message)
    } catch (exception) {
      console.error(exception)
      channel.send(`Произошла какая-то ошибка. ${admin}, разберись!`)
    }
  })
}
