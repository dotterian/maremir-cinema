import { RichEmbed, Message, TextChannel } from 'discord.js';
import { getNextWednesday } from '../../common/dateTools';
import { getManager } from 'typeorm';
import Film from '../../common/models/Film';
import getPoll from '../../common/getPoll';
import { declOfNum } from '../../common/utils';

export default async ({ channel }: Message | { channel: TextChannel }) => {
  const manager = getManager()
  let embed: RichEmbed = null
  const today = new Date()
  const [date, deltaDays] = getNextWednesday()
  let film = await manager.createQueryBuilder(Film, 'film')
    .orderBy('date_watched', 'DESC')
    .getOne()

  const poll = {
    nextDate: date,
    pollActive: false,
    pollResult: {
      today: deltaDays === 0 && today.getHours() >= 20,
      ...film
    },
    answers: [],
    votedIds: []
  }

  if (today.getDay() <= 3 || today.getDay() >= 6) {
    if (today.getDay() !== 3 || today.getHours() < 20) {
      poll.pollActive = true;
      const { answers } = await getPoll(date)
      poll.answers = answers
    }
  }

  if (!poll.pollActive) {
    if (poll.pollResult.today) {
      channel.send('@here')
      embed = new RichEmbed({
        title: `КИНО, УЖЕ СЕГОДНЯ, В 21:00 МСК`,
        description: `\nСмотрим **«${poll.pollResult.title}»**.\n\n→ https://maremir.dotterian.ru/watch ←`,
        url: 'https://maremir.dotterian.ru/watch'
      })
      channel.send(embed)
    } else {
      channel.send(`@here напоминаю, что **по средам в 21:00 МСК** мы смотрим кино.\nГолосование начнётся в субботу.\nЧтобы предложить свой фильм напишите в чат \`!addfilm ссылка_на_страницу_фильма_на_кинопоиске\``)
    }
  } else {
    channel.send('@here')
    let description = 'Напоминаю, что **по средам в 21:00 МСК** мы смотрим кино. Выбор фильма остаётся за вами!\n\nРезультаты голосования на данный момент:\n\n'
    const ordinal = declOfNum(['голос', 'голоса', 'голосов'])
    poll.answers.sort((a, b) => {
      return (a.votes < b.votes) ? 1 : (a.votes > b.votes) ? -1 : 0
    })
    for (let i in poll.answers) {
      if (poll.answers.hasOwnProperty(i)) {
        let answer = poll.answers[i]
        let number = ordinal(answer.votes)
        description += `**${answer.title}** — ${answer.votes} ${number}\n`
      }
    }
    description += `\n\n→ https://maremir.dotterian.ru ←`
    embed = new RichEmbed({
      title: `Проголосовать можно кликнув по этой ссылке!`,
      description,
      url: 'https://maremir.dotterian.ru'
    })
    channel.send(embed)
  }
}
