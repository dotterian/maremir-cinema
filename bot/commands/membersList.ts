import { Message } from 'discord.js';
import { declOfNum } from '../../common/utils';

export default ({ guild, channel }: Message, next: () => void) => {
  const ordinal = declOfNum(['особь', 'особи', 'особей'])
  let number = ordinal(guild.memberCount)
  const onlineUsers = guild.members.filterArray(member => (member.presence.status !== 'offline'))
  let number2 = ordinal(onlineUsers.length)
  let string = `Всего на сервере: **${guild.memberCount}** ${number} (в сети **${onlineUsers.length}** ${number2}) \nСреди них:\n`
  const blackList = [
    '420264764607037440', // стикеробот
    '426834203792113665', // Rythm
    '426899008431194122', // DJ
    '427794667891982356', // Rythm 2
    '428621407346360320', // Titan
    '437936401535860736' // Test group on Dotterian's server (Trello)
  ]

  for (let group of guild.roles) {
    if (group[1].name !== '@everyone' && !blackList.includes(group[1].id)) {
      let number = ordinal(group[1].members.array().length)
      const onlineUsers = group[1].members.filterArray(member => (member.presence.status !== 'offline'))
      let number2 = ordinal(onlineUsers.length)
      string += `— **${group[1].name}**: __${group[1].members.array().length}__ ${number} (в сети __${onlineUsers.length}__ ${number2})\n`
    }
  }
  channel.send(string)
}
