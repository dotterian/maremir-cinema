import { GuildMember, RichEmbed, TextChannel } from 'discord.js';
import User from "../../common/models/User";
import {getManager} from "typeorm";

export default async (member: GuildMember) => {
  const manager = getManager()
  const channel = member.guild.channels.find('name', 'барицентр')
  if (!channel || !(channel instanceof TextChannel)) {
    return
  }

  const user = await manager.findOne(User, {
    guid: member.id
  })

  const embed = new RichEmbed({
    title: `Добро пожаловать в «Барицентр», ${member.displayName}!`,
    description: `Кликнув по заголовку этого сообщения ты сможешь больше узнать о нашем чате.`,
    url: `https://maremir.dotterian.ru/page/links`
  })
  embed.setImage('https://maremir.dotterian.ru/thumbsup.png')

  if (user) {
    const phrases = [
      `Добро пожабровать, ${user.name}. Снова.`,
      `О! Привет ${user.name}, давно не виделись!`,
      `Ну сколько можно туда-сюда ходить-то, а, ${user.name}?`,
      `С возвращением, ${user.name}. Сколько лет, сколько рыб.`
    ]
    embed.setTitle(phrases[Math.round(Math.random() * 3)])
    embed.setDescription(`Ты, конечно, в курсе, но напоминаю, что там по ссылке в заголовке можно всякое интересное про чат почитать.`)
    embed.setImage('https://maremir.dotterian.ru/img/wat.png')
  }

  embed.setColor('RANDOM')
  channel.send(embed)
}
