import {Message} from "discord.js";
import {randomArrayElement} from "../../common/utils";

export default ({channel}: Message) => {
  const images = [
    'blamekadets.png',
    'blamekoyotl.png',
    'fishfodder.png'
  ]

  const random = randomArrayElement(images)
  channel.send(`https://maremir.dotterian.ru/${random}`)
}
