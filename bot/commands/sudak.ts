import { Message } from 'discord.js'
import { randomArrayElement } from '../../common/utils';
import { readFile, writeFile } from 'async-file'
import { format } from 'date-fns'
import { getManager } from 'typeorm';
import User from '../../common/models/User';

const randomPhrase = (id: number) => {
  const phrases = [
    // Initial phrases
    [
      'Инициирую поиск судака дня…',
      'Ну? Кто себя плохо вёл?',
      'Активирую судацкий сканер…',
      'Дырявлю икринку…'
    ],
    // Gotcha!
    [
      'Да это же %username%! Вот он, держите судака!',
      'Так и знала, что %username% тот ещё судак!',
      'И почётным судаком дня объявляется %username%. Пройдите в корпус за наградой.',
      'Это %username%! Как это нет? Судацкий сканер так не работает.',
      'Тихо на дне, только не спит %username%. Знает %username%, что он тот ещё судак, вот и не спит %username%.',
      'Судак судака видит издалека, правда %username%?'
    ],
    // My information says
    [
      'Путём демократичных выборов судаком дня был выбран %username%',
      'Я уже всё подсчитала. Судак — %username%.',
      'Надёжные источники докладывают: %username% первосортный судачище',
      'Знакомьтесь, его судачейшество %username%'
    ]
  ]
  return randomArrayElement(phrases[id])
}


export default async ({ guild, channel }: Message) => {
  let sudak = await readFile('./sudak.json')
  sudak = JSON.parse(sudak)

  if (typeof sudak[guild.name] === 'undefined') {
    sudak[guild.name] = {
      date: null,
      sudak: null
    }
  }

  if (sudak[guild.name].date !== format(new Date(), 'DD.MM.YYYY')) {
    /*const randomUser = guild.members.random()*/
    const manager = getManager()
    const users = await manager.getRepository(User)
      .createQueryBuilder('user')
      .where("words_total > 2000 AND today > date('now', '-3 month')")
      .orderBy('random()')
      .getMany();

    const randomUser = users.find(function (user) {
      return guild.members.has(user.guid)
    });
    channel.send(randomPhrase(0))
    sudak[guild.name].date = format(new Date(), 'DD.MM.YYYY')
    sudak[guild.name].sudak = `<@!${randomUser.guid}>`
    setTimeout(() => {
      let string = randomPhrase(1)
      string = string.replace(/%username%/g, sudak[guild.name].sudak)
      channel.send(string)
    }, 3000)
    await writeFile('./sudak.json', JSON.stringify(sudak))
    randomUser.sudak_counter = (randomUser.sudak_counter || 0) + 1
    await manager.save(randomUser)
  } else {
    let string = randomPhrase(2)
    string = string.replace(/%username%/g, sudak[guild.name].sudak)
    channel.send(string)
  }
}
