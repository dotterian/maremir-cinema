import { formatDateSql } from '../../common/dateTools';
import Voting, { FilmInfo } from '../../common/models/Voting';
import { randomArrayElement } from '../../common/utils';
import Film from '../../common/models/Film';
import { TextChannel, Message } from 'discord.js';
import { getManager } from 'typeorm';

export const endVoteMiddleware = ({ channel }: Message) => {
  if (channel instanceof TextChannel) {
    endVote(channel)
  }
}

const endVote = async (channel: TextChannel) => {
  const watchedDate = formatDateSql(new Date())
  const manager = getManager()
  const poll = await manager.findOne(Voting, {
    order: {
      id: 'DESC'
    }
  })
  const films = poll.films
  let topFilm: FilmInfo = { film_id: null, user_id: null, votes: 0 }
  let topFilms: FilmInfo[] = []
  films.map(film => {
    if (film.votes > topFilm.votes) {
      topFilm = film
      topFilms = [
        film
      ]
    } else if (film.votes === topFilm.votes) {
      topFilms.push(film)
    }
  })

  if (topFilms.length > 0) {
    topFilm = randomArrayElement(topFilms)
  }

  const film = await manager.findOne(Film, topFilm.film_id, {
    relations: ['user']
  })

  await manager.update(Film, topFilm.film_id, {
    watched: true,
    date_watched: watchedDate
  })

  channel.send(`@here голосование за фильм завершено:\nПобедитель: **${film.title}**\nПредложил${film.user.gender !== true ? 'a' : ''}: <@${film.user_uuid}>\n\nНачало сеанса через час, в 21:00 МСК.\nhttps://maremir.dotterian.ru`)
}

export default endVote
