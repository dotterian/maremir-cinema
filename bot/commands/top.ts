import { TextChannel } from 'discord.js';
import User from '../../common/models/User';
import { declOfNum, declofNumSubFunction } from '../../common/utils';
import { formatDateSql } from '../../common/dateTools';
import { getManager, SelectQueryBuilder } from 'typeorm';
import { MiddlewareFunction } from '../utils/middleware';

type KeysOfType<Base, Condition> = {
  [Key in keyof Base]: Base[Key] extends Condition ? Key : never
}[keyof Base]

type TopParams = {
  param: KeysOfType<User, number>,
  header: string,
  units: declofNumSubFunction,
  query: SelectQueryBuilder<User>,
  channel: TextChannel
}

type TopPartialParams = {
  param: KeysOfType<User, number>,
  header: string,
  units?: declofNumSubFunction,
  query?: SelectQueryBuilder<User>
  channel: TextChannel
}

export const DailyTop = (channel: TextChannel): void => {
  const manager = getManager()
  const today = formatDateSql(new Date())
  const query = manager.createQueryBuilder(User, 'user')
    .where({
      today
    })
  top({
    param: 'words_today',
    header: 'Топ пользователей за сегодня',
    query,
    channel
  })
}

export const OverallTop = (channel: TextChannel): void => {
  top({
    param: 'words_total',
    header: 'Топ польователей за всё время',
    channel
  })
}

export const SudakTop = (channel: TextChannel): void => {
  top({
    param: 'sudak_counter',
    header: 'Топ судаков дня',
    units: declOfNum(['раз', 'раза', 'раз']),
    channel
  })
}

export const TopController: MiddlewareFunction = ({ content, channel }, next) => {
  const words = content.split(' ')
  if (!(channel instanceof TextChannel)) {
    next()
    return
  }
  if (['all', 'все', 'всё'].includes(words[1])) {
    OverallTop(channel)
    return
  }
  if (['судак', 'sudak'].includes(words[1])) {
    SudakTop(channel)
    return
  }
  DailyTop(channel)
}

const top = async (params: TopPartialParams) => {
  const manager = getManager()
  const options: TopParams = {
    units: declOfNum(['слово', 'слова', 'слов']),
    query: manager.createQueryBuilder(User, 'user'),
    ...params
  }
  const {
    param,
    header,
    units,
    query,
    channel
  } = options
  const users = await query
    .andWhere(`${param} >= 1`)
    .orderBy(`user.${param}`, 'DESC')
    .limit(10)
    .getMany()

  let string = `**${header}:**\n`
  users.map((user: User, id: number) => {
    string += `${id + 1}. ${user.name} — ${user[param]} ${units(user[param])}\n`
  })

  if (!users.length) {
    string += `-- Топ пуст --`
  }

  channel.send(string)
}
