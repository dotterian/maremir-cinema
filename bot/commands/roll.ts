import { Message } from 'discord.js';
import fetch from 'node-fetch';

export default ({ content, channel }: Message, next: () => void) => {
  const regex = /!(roll|ролл)( +((\d+)? *(d|д) *)?(\d+)( *(-|\+) *(\d+))?)?/i
  const regexYesNo = /!(roll|ролл) +(монетка|coin)/i
  const easterRegex = /!(roll|ролл) (roll|ролл|суши)/i

  if (regexYesNo.test(content)) {
    const string = Math.round(Math.random()) ? 'Да' : 'Нет'
    channel.send(string)
    return
  }

  if (easterRegex.test(content)) {
    fetch('https://loremflickr.com/640/480/sushi')
      .then(response => {
        channel.send(response.url)
      })
    return
  }

  let diceNumber = 1
  let diceSize = 20
  let deltaSign = '-'
  let delta = 0
  const result = regex.exec(content)
  if (result[2]) {
    if (result[4]) {
      diceNumber = parseInt(result[4])
    }
    if (result[6]) {
      diceSize = parseInt(result[6])
    }
    if (result[7]) {
      deltaSign = result[8]
      delta = parseInt(result[9])
    }
  }

  if (diceSize < 2) {
    channel.send('У вашего кубика маловато граней!')
    return
  }

  if (diceSize > 1000) {
    channel.send('Сами катайте ваши шары!')
    return
  }

  if (diceNumber < 1) {
    channel.send('*бросает на стол пустоту*')
    return
  }

  if (diceNumber > 100) {
    channel.send('А не многовато кубов?! Не?')
    return
  }

  let string = `**Бросок:** ${diceNumber}d${diceSize}${delta>0 ? ` ${deltaSign}${delta}` :''}\n**Результат:** `
  const results = []
  let total = 0
  for (let i = 0; i < diceNumber; i++) {
    let res = 1 + Math.round(Math.random() * (diceSize - 1))
    results.push(res)
    total += res
  }
  if (results.length === 1) {
    string += results[0] + ' '
  } else {
    string += '(' + results.join(' + ') + ') '
  }
  if (delta > 0) {
    string += `${deltaSign} ${delta} `
    total += deltaSign === '+' ? delta : -delta
  }
  if (results.length > 1 || delta > 0) {
    string += '= ' + total
  }
  channel.send(string)
}
