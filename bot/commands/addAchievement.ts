import { Message } from 'discord.js';
import * as pureimage from 'pureimage'
import * as request from 'request'
import { createReadStream, createWriteStream } from 'fs';
import { getManager } from 'typeorm';
import Achievement from '../../common/models/Achievement';
import { format } from 'date-fns';

function wrapText(context, text, x, y, maxWidth, lineHeight) {
  const lines = text.split('\n')
  let line = ''
  for (let i = 0; i < lines.length; i++) {
    const words = lines[i].split(' ')

    for (let n = 0; n < words.length; n++) {
      const testLine = line + words[n] + ' '
      const metrics = context.measureText(testLine)
      const testWidth = metrics.width
      if (testWidth > maxWidth && n > 0) {
        context.fillText(line, x, y)
        line = words[n] + ' '
        y += lineHeight
      } else {
        line = testLine
      }
    }
    context.fillText(line, x, y)
    y += lineHeight
    line = ''
  }
}

export default async ({ content, channel, attachments, member }: Message) => {
  const params = content.split(' ')
  const regex = /^![^ ]+ +<@!?(\d+)> +"([^"]+)" +?"?([^"]+)?"?/
  const result = regex.exec(content)
  if (result === null) {
    channel.send(`Правильный формат команды:\n\`\`\`\n${params[0]} @юзернейм "Название ачивки" "Описание ачивки"\n\`\`\`\nПри желании, описание можно опустить, но я рекомендую не лениться и описать т.к. потом достижение может и забыться.\nЕсли прикрепить к команде изображение, то оно будет вклеено в ачивку. Рекомендую вклеивать квадратные изображения. Все остальные будет плющить.`)
    return
  }

  const user = result[1]
  const achievementName = result[2]
  const achievementDescription = result[3]
  let image = null
  let getter = null
  const manager = getManager()

  if (attachments.first() && attachments.first().url) {
    const attach = attachments.first()
    if (attach.url.endsWith('.png')) {
      getter = pureimage.decodePNGFromStream
    }
    if (attach.url.endsWith('.jpg') || attach.url.endsWith('.jpeg')) {
      getter = pureimage.decodeJPEGFromStream
    }
    image = request(attach.url)
  }
  if (getter === null) {
    getter = pureimage.decodePNGFromStream
    image = createReadStream('./bot/img/achievementIcon.png')
  }
  const achievement = manager.create(Achievement, {
    user: user,
    name: achievementName,
    text: achievementDescription,
    given_by: member.id,
    given_at: format(new Date(), 'DD.MM.YYYY H:mm')
  })
  await manager.save(achievement)
  console.log(achievement.id)
  const [img, img2] = await Promise.all([
    pureimage.decodePNGFromStream(createReadStream('./bot/img/achievement.png')),
    getter(image)
  ])
  const font1 = pureimage.registerFont('client/fonts/10858-webfont.ttf', 'Atlantic Cruise')
  const font2 = pureimage.registerFont('client/fonts/v_blambotcasual_v1.0-webfont.ttf', 'BB')
  await new Promise((resolve => font1.load(() => {
    font2.load(() => {
      resolve()
    })
  })))

  const c = img.getContext('2d')
  c.drawImage(img2,
    0, 0, img2.width, img2.height,
    41, 30, 178, 178
  )
  c.fillStyle = '#464C58'
  c.font = '48pt \'Atlantic Cruise\''
  c.fillText(achievementName, 250, 65)
  c.fillStyle = '#72777f'
  c.font = '25pt BB'
  wrapText(c, achievementDescription, 250, 100, 550, 30)
  await pureimage.encodePNGToStream(img, createWriteStream(`./bot/achievements/${achievement.id}.png`))
  channel.send(`<@${user}> получил(а) ачивку:`, { files: [`https://maremir.dotterian.ru/achievements/${achievement.id}.png`] })
}
