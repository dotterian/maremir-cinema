import {Message} from "discord.js";
import YouTube from 'simple-youtube-api';
import ytdl from 'ytdl-core';

const RadioChannelID = process.env.RADIO_CHANNEL_ID || '550766316433178629';
const AdminRoleID = process.env.ADMIN_ROLE_ID || '322756907104206858';


enum commands {
  play = '!play',
  skip = '!skip',
  stop = '!stop',
  np = '!np',
  queue = '!queue',
  pause = '!pause',
  resume = '!resume',
  remove = '!remove'
}

export default {
  commands: Object.keys(commands).map(k => commands[k]),
  middleware: (message: Message, next: () => void) => {
    const wordsArray = message.content.split(' ');
    switch (wordsArray[0]) {
      case commands.play:
        if (this.checkMusicChannel(message)) {
          this.playMusic(message)
        }
        break;
      case commands.skip:
        if (this.checkMusicChannel(message) && this.checkAdminOrAlone(message)) {
          this.skipTrack()
        }
        break;
      case commands.stop:
        if (this.checkMusicChannel(message) && this.checkAdminOrAlone(message)) {
          this.clearPlaylist()
        }
        break;
      case commands.pause:
        if (this.checkMusicChannel(message) && this.checkAdminOrAlone(message)) {
          this.pause()
        }
        break;
      case commands.resume:
        if (this.checkMusicChannel(message)) {
          this.resume()
        }
        break;
      case commands.remove:
        if (this.checkMusicChannel(message)) {
          this.remove(message)
        }
        break;
      case commands.np:
        this.nowPlaying(message);
        break;
      case commands.queue:
        this.queue(message);
        break;
      default:
        next();
        break;
    }
  },
  checkMusicChannel: (message: Message) => {
    if (!message.member.voiceChannel) {
      message.channel.send('Сперва войди в голосовой канал!');
      return false
    }
    if (message.member.voiceChannelID !== RadioChannelID) {
      message.channel.send('Слушать музыку можно только в «Маревом Радио»!');
      return false
    }
    return true
  },
  checkAdminOrAlone: (message: Message) => {
    const voiceMembers = message.member.voiceChannel.members.filter(member => (member.id !== message.member.id && member.id !== this.bot.user.id));
    if (voiceMembers.size === 0) {
      return true
    }
    if (!message.member.roles.has(AdminRoleID)) {
      message.channel.send('Ты не админ, тебе нельзя!');
      return false
    }
    return true
  },
  playMusic: async (message: Message) => {
    const args = message.content.split(' ');
    const searchString = args.slice(1).join(' ');
    const url = args[1] ? args[1].replace(/<(.+)>/g, '$1') : '';
    let video;
    if (url.match(/^https?:\/\/(www.youtube.com|youtube.com)\/playlist(.*)$/)) {
      return message.channel.send('Никаких плейлистов!');
    }
    try {
      video = await youtube.getVideo(url);
    } catch (error) {
      try {
        const videos = await youtube.searchVideos(searchString, 10);
        let index = 0;
        message.channel.send(`__**Выбор трека:**__\n${videos.map(video2 => `**${++index} -** ${video2.title}`).join('\n')}\nОтправьте в ответ номер желаемого трека.`);
        try {
          const response = await message.channel.awaitMessages(msg2 => msg2.content > 0 && msg2.content < 11, {
            maxMatches: 1,
            time: 10000,
            errors: ['time']
          });
          const videoIndex = parseInt(response.first().content);
          video = await youtube.getVideoByID(videos[videoIndex - 1].id);
        } catch (err) {
          console.error(err);
          return message.channel.send('Я так и не получила ответа. Отменяю поиск музыки.');
        }
      } catch (err) {
        console.error(err);
        return message.channel.send('Обошла весь ютуб и ничего не нашла. :usuk:');
      }
    }
    return this.handleVideo(video, message, message.member.voiceChannel);
  },
  handleVideo: async (video, msg, voiceChannel, playlist = false) {
    const serverQueue = queue.get(msg.guild.id);
    console.log(video);
    const song = {
      id: video.id,
      title: Util.escapeMarkdown(video.title),
      url: `https://www.youtube.com/watch?v=${video.id}`
    };
    if (!serverQueue) {
      const queueConstruct = {
        textChannel: msg.channel,
        voiceChannel: voiceChannel,
        connection: null,
        songs: [],
        volume: 5,
        playing: true
      };
      queue.set(msg.guild.id, queueConstruct);

      queueConstruct.songs.push(song);

      try {
        var connection = await voiceChannel.join();
        queueConstruct.connection = connection;
        play(msg.guild, queueConstruct.songs[0]);
      } catch (error) {
        console.error(`I could not join the voice channel: ${error}`);
        queue.delete(msg.guild.id);
        return msg.channel.send(`I could not join the voice channel: ${error}`);
      }
    } else {
      serverQueue.songs.push(song);
      console.log(serverQueue.songs);
      if (playlist) return undefined;
      else return msg.channel.send(`✅ **${song.title}** has been added to the queue!`);
    }
    return undefined;
  }

}
