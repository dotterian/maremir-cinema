import { MiddlewareFunction } from '../utils/middleware';
import { getManager } from 'typeorm';
import User from '../../common/models/User';
import { formatDateSql } from '../../common/dateTools';

const logActivity: MiddlewareFunction = async ({ content, member }, next) => {
  const wordCount = content.split(' ').length
  const manager = getManager()
  let user: User
  try {
    user = await manager.findOne(User, member.id.toString())

    if (!user) {
      console.log(`Creating new user ${member.user.username}`)
      user = manager.create(User, {
        guid: member.id,
        words_today: 0,
        words_total: 0,
        sudak_counter: 0
      })
      await manager.save(user)
    }

    const date = formatDateSql(new Date())
    const name = member.user.lastMessage.member.nickname || member.user.username
    if (user.name !== name) {
      user.name = name
    }
    if (user.today !== date) {
      user.today = date
      user.words_today = 0
    }
    user.words_today += wordCount
    user.words_total += wordCount
    await manager.save(user)
    next()
  } catch (e) {
    console.log(e)
  }
}

export default logActivity
