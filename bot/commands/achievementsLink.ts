import { Message } from 'discord.js';
import { getManager } from 'typeorm';
import Achievement from '../../common/models/Achievement';
import { declOfNum } from '../../common/utils';
import User from '../../common/models/User';

export default async ({ channel, member, content }: Message) => {
  const regex = /![^ ]+ +<@!?(\d+)>/
  const result = regex.exec(content)
  const manager = getManager()
  let guid: string
  if (!result) {
    guid = member.id
  } else {
    guid = result[1]
  }

  const user = await manager.findOne(User, guid, {
    relations: ['achievements']
  })
  const ordinal = declOfNum(['ачивку', 'ачивки', 'ачивок'])
  let units = ordinal(user.achievements.length)
  let message = `${user.name} заслужил${user.gender ? '' : 'а'} ${user.achievements.length} ${units}.\nПосмотреть все ачивки можно тут:\nhttps://maremir.dotterian.ru/users/${guid}`
  if (user.achievements.length === 0) {
    message = `${user.name} не заслужил${user.gender ? '' : 'a'} ачивок.`
  }
  channel.send(message)
}
