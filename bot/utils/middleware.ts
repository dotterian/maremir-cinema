import { Message, Client } from 'discord.js';

type TriggerFunction = (message: Message) => boolean

type TriggerType = true | string | string[] | TriggerFunction

export type MiddlewareFunction = (message: Message, next?: () => void) => void

export default class Middleware {

  protected fun = false
  protected bot: Client
  protected message: Message
  protected middlewares: [TriggerType, MiddlewareFunction][] = []

  protected next = (current: number) => {
    let middleware = this.middlewares[current]
    if (!middleware) {
      return
    }
    let fn: MiddlewareFunction = middleware[1].bind(this)

    if (this.checkConditions(middleware[0])) {
      return fn(this.message, () => this.next(++current))
    }
    this.next(++current)
  }

  public resolve = (bot: Client, message: Message) => {
    this.bot = bot
    this.message = message
    this.fun = false
    return this.next(0)
  }

  public use = (trigger: TriggerType, middleware: MiddlewareFunction) => {
    let middlewares = this.middlewares.slice()
    middlewares.push([trigger, middleware])

    this.middlewares = middlewares
    return this
  }

  protected checkConditions = (trigger: TriggerType): boolean => {
    if (trigger === true) {
      return true
    }
    const wordsArray = this.message.content.split(' ');
    if (typeof trigger === 'string') {
      return (wordsArray[0] === trigger)
    } else if (Array.isArray(trigger)) {
      return trigger.includes(wordsArray[0])
    }
    const fn = trigger.bind(this)
    return fn(this.message)
  }
}
