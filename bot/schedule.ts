import { Client, TextChannel } from 'discord.js'
import { RecurrenceRule, scheduleJob, JobCallback } from 'node-schedule'
import endVote from './commands/endVote'
import { DailyTop } from './commands/top'
import MoviePoll from './commands/poll'

const channelId = process.env.CHANNEL_ID || '436311519341576212'

type TimeOptions = {
  dayOfWeek?: number,
  hour?: number,
  minute?: number,
  second?: number
}

const ScheduleEvent = (timeOptions: TimeOptions, job: JobCallback) => {
  const rule = new RecurrenceRule()

  for (let i in timeOptions) {
    if (timeOptions.hasOwnProperty(i)) {
      rule[i] = timeOptions[i]
    }
  }

  scheduleJob(rule, job)
}

export default (bot: Client) => {
  const channel = bot.channels.get(channelId)

  if (!((channel): channel is TextChannel => channel.type === 'text')(channel)) return;

  // End vote every wednesday at 20:00
  /*ScheduleEvent({
    dayOfWeek: 3,
    hour: 20,
    minute: 0
  }, async () => {
    await endVote(channel)
  })*/

  // Голосоватб
  ScheduleEvent({
    hour: 11,
    minute: 0
  }, () => {
      channel.send('@here не забываем голосовать за наш любимый комикс!\n http://top.a-comics.ru/voter.php/?q=ZA&cid=704')
  })

  ScheduleEvent({
    hour: 23,
    minute: 59,
    second: 59
  }, async () => {
    await DailyTop(channel)
  })

  /*ScheduleEvent({
    dayOfWeek: 6,
    hour: 10,
    minute: 0
  }, async () => {
    await MoviePoll({ channel })
  })*/
}
