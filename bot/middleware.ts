import Middleware from './utils/middleware'
import logActivity from './commands/logActivity'
import { randomArrayElement } from '../common/utils';
import { TopController } from './commands/top';
import membersList from './commands/membersList';
import poll from './commands/poll';
import sudak from './commands/sudak';
import addfilm from './commands/addfilm';
import addAchievement from './commands/addAchievement';
import achievementsLink from './commands/achievementsLink';
import roll from './commands/roll';
import eatenBy from "./commands/eatenBy";
// import Music from './commands/music';
// import { endVoteMiddleware } from './commands/endVote';

const middleware = new Middleware()

middleware
  // Check that user is member
  .use(true, (message, next) => {
    if (message.member) {
      next()
    }
  })
  // Check that user is not Ostrica
  .use(true, function (message, next) {
    if (message.member.id !== this.bot.user.id) {
      next()
    }
  })
  // Help
  // .use(['!commands', '!команды', '!помощь', '!help'], showHelp)
  // Members
  .use('!members', membersList)
  // Топ
  .use(['!топ', '!top'], TopController)
  // Показать ссылку на сование голым
  .use('!vote', ({ channel }) => {
    channel.send('@here не забываем голосовать за наш любимый комикс! http://top.a-comics.ru/voter.php/?q=ZA&cid=704')
  })
  // Показать голосовалку за кино
  .use(['!кино', '!kino'], poll)
  // Выбираем судака
  .use(['!sudak', '!судак'], sudak)
  // Добавить фильм
  .use(['!addfilm', '!предлагаю'], addfilm)
  // Добавить ачивку
  .use(['!achievement', '!ачивка'], addAchievement)
  .use(['!achievements', '!ачивки'], achievementsLink)
  // Ролл!
  .use(['!roll', '!ролл'], roll)
  //.use('!endVote', endVoteMiddleware)
  // Ссылки на ссылки
  .use(['!links', '!ссылки'], ({ channel }) => {
    channel.send('Сайт чатика: https://maremir.dotterian.ru\nПолезные ссылки: https://maremir.dotterian.ru/page/links\nИнструкция к кофеварке: https://maremir.dotterian.ru/page/bot')
  })
  // Мирон
  .use(['!miron', '!мирон'], ({ channel }) => {
    channel.send('Голавль, запрора, палтус, хек\nДорадо, язь, белуга\nГаррупа, шпрота, жерех, чир\nЕлец, паку… мурена\nРогозуб, сардина\nОсётр, тельматерина\nЩука, мерлангище, буффалище, пескарь, рачила\nПайче, плотва, гнюс, кабуба\nГуппи, медака, пампано, сайда\nАргус, нарцина, путассу, гампала\nНавага, умбрина, севрюга, сарда… РАУНД!')
  })
  // Её съели…
  .use(function ({ content }) {
    const wordsArray = content.split(' ');
    let found = ['!blame', '!съели', '!eatenby', 'её съели'].includes(wordsArray[0])

    if (!found) {
      found = /^е(ё|го) съели?/i.test(content)
    }

    return found
  }, eatenBy)
  // Музыка
  // .use(Music.commands, Music.middleware)
  // ГИТЛЕР!
  .use(function ({ content }) {
    return /^а знае(те|шь)(,)? кто ещ[ёе]/i.test(content)
  }, function ({ channel }, next) {
    this.fun = true;
    channel.send('Гитлер!')
    next()
  })
  // Невероятная воля
  .use(function ({ content, member }) {
    return (content.length > 1000) && (member.id === '418425332107051009') && !this.fun
  }, function ({ channel, member }, next) {
    this.fun = true
    channel.send(`Невероятная воля, ${member}. Впечатляющее достоинство.`)
    next()
  })
  // Нытьё
  .use(function ({ content }) {
    return (content.length > 300) && (/(по)?н(ыть|ою)[ёея]?/i.test(content)) && !this.fun
  }, function ({ channel }, next) {
    this.fun = true
    const images = ['koshappy', 'koshi', 'koslalala', 'kosrruuu']
    let image = randomArrayElement(images)
    channel.send('— РРРРРРУУУУУУУУУ~', { files: [`https://maremir.dotterian.ru/${image}.png`] })
    next()
  })
  .use(function(message) {
    return message.isMemberMentioned(this.bot.user)
  }, function ({ channel }, next) {
    const phrases = [
      'Вероятность подслушивания: нулевая. Уточнение: не ничтожно малая. Математически нулевая.',
      'Состояние: Зрачки расширены. Дыхание учащённое. Нервная система возбуждена.',
      'Причины: а) полынь, б) нарушение циркадного цикла ввиду нерегулярного сна, в) зависть',
      'Разъяснение: недоступно',
      'Причина фрустрации объекта: некачественная психика.',
      'Обветшалость. Автоматизм. Бракованный импульс.',
      'Центр внимания: юноша с ножом.',
      'Желание тела: соблюдать статус-кво. Сидеть на месте. Делать привычное.',
      'Исход: безделье и фрустрация.',
      'Внешний советник: отсутствует.',
      'Внутренний советник: некачественный.',
      'Брак души.',
      'Совет: не уоминать рыб.',
      'Дурман. Пьянство. Отупение. Ленность. Ленность.',
      'Позиция: высказана.',
      'Совет: оставить в покое.',
      'Анализ: острое и дурное эмоциональное состояние.',
      'Рекомендация: химическое воздействие на эмоции.',
      'Приоритет: тактический комфорт. Стратегический: локально не существенен.',
      'Стратегический комфорт достижим при наличии благоприятных условий. Тактический дискомфорт — препятствие.',
      'Первостепенная задача — устранить тактический дискомфорт.',
      'Комментарий: Объект является зрелым представителем своего вида. Высказанное пожелание отражает его реальные потребности.',
      'Справка: Всякий предмет беседы называется её объектом. Это не оценочное высказывание.',
      'Внимание! Предупреждение! Объект относится к типу людей, отличающихся повышенной любвеобильностью при интоксикации.',
      'Настоятельный совет: не соглашаться',
      'Вероятность того, что объект скоро поменяет позицию: высокая.',
      'Очень настоятельный совет: не ходить.',
      'Ошибка. Свойства объекта: Несущественны.',
      'Цель: Защита имаго. Как и было обещано.',
      'Этот индивид гарантировал имаго защиту в сферах, которые не являются его сильными сторонами.',
      'Речь не о физической угрозе. Речь об угрозе психике имаго.',
      'Имаго будет ранен. Растоптан. Унижен.',
      'Имаго отсанется здесь.',
      'Анализ: имаго рассчитывает найти полового партнёра.',
      'Имаго никогда не был привлекателен для особей своего вида.',
      'Ввиду рода занятий количество социальных контактов имаго аномально высоко, но он не получает сексуальных предложений.',
      'Предложение вызвано не реальным интересом, а химически изменённым состоянием организма.',
      'Такой вариант возможен. И это худший вариант.',
      'Несмотря на возраст объект имеет ограниченный опыт личных отношений.',
      'Объект не умеет предугадывать, какие черты партнёра ему на самом деле комфортны.',
      'Объект влюблён в роль, которую играет имаго, и не подвергает реальному анализу его черты.',
      'Объект разочаруется, как только в деталях разберётся, кто перед ним.',
      'Объект никогда не видел имаго голым. Больным. В дурных биологических кондициях. В процессе обратной перистальтики.',
      'Рохля. Заика. Трус. Девственник.'
    ]
    let phrase = randomArrayElement(phrases)
    channel.send(phrase)
    next()
  })
  // Common commands
  .use(true, logActivity)

export default middleware
