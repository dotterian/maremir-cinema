import { Server as RpcServer } from 'node-json-rpc2'
import { RichEmbed, Client, TextChannel } from 'discord.js';

const maremirChannelId = process.env.MAREMIR_CHANNEL_ID || '436311519341576212'
const debugChannelId = process.env.DEBUG_CHANNEL_ID || '436311519341576212'

export default (server: RpcServer, bot: Client) => {
  const maremirChannel = bot.channels.get(maremirChannelId)
  const debugChannel = bot.channels.get(debugChannelId)

  server.addMethod('newPage', (params) => {
    const embed = new RichEmbed({
      title: 'Новая страница!',
      description: `${params[1]}\n${params[0]}`,
      url: params[0]
    })
      .setColor('RANDOM')
      .setImage(params[2])

    if (!((maremirChannel): maremirChannel is TextChannel => maremirChannel.type === 'text')(maremirChannel)) return;

    maremirChannel.send('@here')
    maremirChannel.send(embed)
  })

  server.addMethod('vote', (params) => {

    if (!((debugChannel): debugChannel is TextChannel => debugChannel.type === 'text')(debugChannel)) return;

    debugChannel.send(`${params[0]} проголосовал за следующие фильмы:\n${params[1].join('\n')}`)
  })
}
