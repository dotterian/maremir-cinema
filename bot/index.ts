import * as Discord from 'discord.js'
import { Server as RpcServer } from 'node-json-rpc2'
import RpcMethods from './rpc'
import Schedule from './schedule'
import GuildMemberAdd from './commands/GuildMemberAdd'
import createConnection from '../common/database'
import BotMiddleware from './middleware'

const AUTH_TOKEN = process.env.AUTH_TOKEN

createConnection()
  .then(() => {
    const bot = new Discord.Client()
    const rpcOptions = {
      port: 5711,
      host: '127.0.0.1',
      path: '/',
      method: 'POST'
    }

    console.log('##############################')
    console.log('Bot ready')

    bot.on('ready', () => {
      console.log(`Logged in as: ${bot.user.username}#${bot.user.discriminator}`)

      const rpcServer = new RpcServer(rpcOptions)
      RpcMethods(rpcServer, bot)
      Schedule(bot)
    })

    bot.on('message', (message) => {
      BotMiddleware.resolve(bot, message)
    })

    bot.on('guildMemberAdd', GuildMemberAdd)

    bot.login(AUTH_TOKEN)
  })
  .catch(err => {
    console.error(err)
  })
