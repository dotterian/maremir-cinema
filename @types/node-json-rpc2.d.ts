declare module 'node-json-rpc2' {

  type ClientOptions = {
    protocol?: 'http' | 'https',
    user?: string,
    password?: string,
    pass?: string,
    host?: string,
    port?: number,
    path?: string,
  }

  type ServerOptions = {
    protocol?: 'http' | 'https',
    user?: string,
    password?: string,
    pass?: string,
    port?: number,
    host?: string,
  }

  type CallOptions = {
    method?: string,
    params?: any[],
    jsonrpc?: string,
    id?: string
  }

  export class Client {
    constructor(options: ClientOptions)
    call(options: CallOptions, callback: (type: string | Error, data: any) => void, id?: string): void
  }

  export class Server {
    constructor(options: ServerOptions)
    addMethod(methodName: string, callback: (...args: any[]) => void): void
  }
}
